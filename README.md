# GIT Statistics - All versions

This repository contains code to generate statistics on contributions to GIT repositories. 

Two separate web services using different techniques:

* [NordlingLab-gitstat](./gitstat_2020/) - New version from 2020
* [NordlingLab-GITStatistics](./GITStatistics_2017/) - Old version from 2017


## Background

The statistics is shown at [statistics.nordlinglab.org](statistics.nordlinglab.org). This code was originally developed for showing 
statistics on contributions to [Nordron-SciInfo](https://bitbucket.org/nordron/nordron-sciinfo) during the 
"Scientific information gathering and processing for engineering research" course. 
