# Setup a gitstat server on CentOS 8


## I. Install CnetOS 8

1. Download [CnetOS8 image](https://www.centos.org/centos-linux/) (`CentOS-8.x.yyyy-oooo-dvd1.iso` is preffered).
2. Make boot USB ([Official doc](https://wiki.centos.org/HowTos/InstallFromUSBkey))

        sudo dnf install lsscsi
        lsscsi       // check USB (>8G)
        dd if=CentOS-6.5-x86_64-bin-DVD1.iso of=/dev/sdz status=progress

3. Install it ([Official doc](https://docs.centos.org/en-US/8-docs/), [tutorial 1](https://linoxide.com/distros/how-to-install-centos/))

        Insert USB => start coumputer => BIOS => change boot order => reboot => install ...

4. Install language pack (optional)

        sudo dnf search langpacks-zh_TW
        sudo dnf install name_of_that_pkg
        reboot

## II. Configure network

1. Set static IP ([tutorial 1](https://linuxconfig.org/rhel-8-configure-static-ip-address), [tutorial 2](https://linuxhint.com/configure_static_ip_centos8/), [tutorial 3](https://www.howtoforge.com/how-to-configure-a-static-ip-address-on-centos-8/))
2. Build SSH server (optional) ([tutorial 1](https://linuxconfig.org/install-ssh-server-on-redhat-8))
3. Restrict IP for SSH server (optional) ([tutorial 1](https://www.cyberciti.biz/faq/match-address-sshd_config-allow-root-loginfrom-one_ip_address-on-linux-unix/?fbclid=IwAR17bt6Lvh-E9FF6iMjcj0geTNp4XowvzivDzySbwiOsMdKhoKZFkqLJpv4))
4. Build Nginx server ([tutorial 1](https://www.footmark.info/linux/centos/centos8-installation-lemp))
5. Set virtual hosts (optional) ([tutorial 1](https://www.tecmint.com/create-nginx-server-blocks-in-centos/), [tutorial 2](https://unihost.com/help/set-up-nginx-virtual-hosts-on-centos-8/)) (If you want to have your own domain name, just buy one)
6. Config SELinux for Nginx server (optional, if you use default settings, then you can skip this) ([tutorial 1](https://www.nginx.com/blog/using-nginx-plus-with-selinux/?fbclid=IwAR17bt6Lvh-E9FF6iMjcj0geTNp4XowvzivDzySbwiOsMdKhoKZFkqLJpv4))

## III. Prepare requirements

1. Install development tools (GNU C, GIT, ...) ([tutorial](https://linuxconfig.org/install-development-tools-on-redhat-8))

        sudo dnf group install "Development Tools"
    
2. Update packages ([tutorial 1](https://linuxhint.com/centos8_package_management_dnf/), [tutorial 2](https://opensource.com/article/18/8/guide-yum-dnf))

        sudo dnf update
   
3. Install libgit2 ([pkg info](https://centos.pkgs.org/8/centos-appstream-x86_64/libgit2-0.26.8-1.el8.x86_64.rpm.html))

        sudo dnf install libgit2
   
4. Install libssh2 1.9.0 ([pkg info](https://centos.pkgs.org/8/epel-x86_64/libssh2-1.9.0-5.el8.x86_64.rpm.html))

        wget https://download-ib01.fedoraproject.org/pub/epel/8/Everything/x86_64/Packages/l/libssh2-1.9.0-5.el8.x86_64.rpm
        sudo rpm -Uvh libssh2-1.9.0-5.el8.x86_64.rpm
        sudo dnf install libssh2

## IV. Install gitstat

1. Upgrade pip3

        sudo pip3 install --upgrade pip

2. Install pygit2

        sudo pip3 install pygit2

3. Install python-dateutil

        sudo pip3 install python3-dateutil
        or
        sudo pip3 install python3-dateutil --upgrade    // if already exists

4. Get gitstat

        cd ~
        mkdir Repositories
        git clone https://bitbucket.org/nordron/nordlinglab-gitstatistics.git


## V. Setup gitstat

1. Generate SSH key for root

        su
        ssh-keygen
        exit

2. Set the public key (`/root/.ssh/xxx.pub`) to the repository or the account
3. Clone the repository you want to use gitstat (by HTTPS or SSH)
4. Ensure your remote's url is consistent to the way you get authenticiation (username/pwd for HTTPS, sshkey for SSH) (usually we use remote `origin`)

        git remote -v

    If not, modify it.

        git remote set-url remote_name remote_url

5. Write config file for gitstat (`config_xxx.json`) (copy from existing and modify)
6. Test for gitstat

        cp xxx/gitstat/gitstat_style.css xxx/www/html     // copy the css file to nginx server
        cp xxx/gitstat/*.png xxx/www/html                 // copy the figures

        sudo generate_total.py config_xxx.json
        or
        sudo generate_durations.py config_xxx.json

7. Let it run automatically by [crontab](https://man7.org/linux/man-pages/man5/crontab.5.html) (use absolute path)

        su
        crontab -e
        0 0,6,12,18 * * * /usr/bin/python3 /xxx/xxx/generate_total.py /xxx/config_xxx.json > path_to_log 2>&1
        exit
