# NordlingLab-gitstat #

**gitstat** is a tool to retrive statistics from a local GIT repository, and visualize the statistics on a web page.
In contrast to **NordlingLab-GITStatistics**, **gitstat** is powered by a more powerful backend -- [**pygit2**](https://www.pygit2.org/), and **gitstat** provides better visualization by [**DataTables**](https://datatables.net/).

* In consideration of performance and maintainance, we changed the backend to **pygit2**.
Using **pygit2** enables us to use git functions more easily.
* For visualization, we use **DataTables** to provide some advanced table functions, such as sorting, searching, pagination, file export, table expand and collpse.
* In addtion, instead of configuring through a Python script, **gitstat** just set up a json file for configuration.

This directory `nordlinglab-gitstatistics/gitstat_2020` contains all the code used to generate statistics on contributions to GIT repositories.
It contains the following:

    .
    |-- html
    |     |-- gitstat_style.css   // CSS style for the webpages
    |     |-- index.html          // navigation page for multiple gitstat webpages
    |     \-- *.png               // logo, snapshots, ...
    |
    |-- gitstat.py                // core functions to obtain information from the repository
    |-- generate_total.py         // get summary of all queries and generate the webpage
    \-- generate_track.py         // get summary of each queries and generate the webpage


## Background

The statistics is shown at [statistics.nordlinglab.org](statistics.nordlinglab.org).
This tool aims to provide similar functions as **NordlingLab-GITStatistics** by a more powerful backend.
(***NordlingLab-GITStatistics** was originally developed for showing statistics on contributions to [Nordron-SciInfo](https://bitbucket.org/nordron/nordron-sciinfo) during the "Scientific information gathering and processing for engineering research" course.*)

## Copyright and license

`NordlingLab-gitstat` is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0) with a kind request to acknowledge the Nordling Lab if you find it useful.
We also appreciate pull requests so your features can be incorporated in this package.

## Dependencies

* Python >= 3.6.0

* [pygit2](https://www.pygit2.org/index.html) >= 1.2.0

    Bindings to the libgit2 shared library, implements Git plumbing.

    *pygit2* is available under the terms [GPLv2 license](https://www.pygit2.org/#license-gplv2-with-linking-exception).

* [python-dateutil](https://dateutil.readthedocs.io/en/stable/index.html) >= 2.7.0

    Powerful extensions to the standard datetime module.

    *python-dateutil* is available under the terms [BSD 3-Clause license](https://dateutil.readthedocs.io/en/stable/#license).

* [DataTables](https://datatables.net) >= 1.10

    Advanced table plug-in for the jQuery Javascript library.

    *DataTables 1.10* and newer is available under the terms [MIT license](https://datatables.net/license/mit).

## Usage

* To install

        git clone <this repository>
        pip3 install python-dateutil
        pip3 install pygit2
        
    To allow authentication through SSH, you need to have libssh2 (>=1.9.0) as mentioned in [the document](https://www.pygit2.org/install.html#quick-install).

* To call the function please use 

        python3 generate_total.py config_xxx.json           // query type == total
        or
        python3 generate_durations.py config_xxx.json       // query type == durations
		
    It will generate an index_xxx.html to the destination folder.

### For setting up a Linux server to run this service, please read [this document](setup_gitstat_centos8.md).


## Configurations

You only need to configure through a json file.

The json files with sensitive information is located in [NordlingLab-SensitiveFiles](https://bitbucket.org/temn/nordlinglab-sensitivefiles/src/master/).

There are two templates for visualizing in different ways.

**Note:** For git clone, if you use HTTPS, you need to input your username and password through terminal;
If you use SSH, you need to associate the SSH key to either your account or the repository.

**Note:** If there are both key: "pubkey" and "privkey", it uses SSH to authenticate. Otherwise, it uses username and password. Besides, please ensure your remote's url is consistent with the way your authentication.

* Query type 1: *total*. To show the summary of all queries.

    Read [this file](config_total.json).

* Query type 2: *durations*. To show the summary for each query.

    Read [this file](config_durations.json).


## Contact

jack.wang@nordlinglab.org
