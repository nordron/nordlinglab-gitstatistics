#!/usr/bin/python
#-*- coding: utf-8 -*-

# Copyright 2017 Nordron AB
# Authors: Wei-Yu Lee, Yu-Sin Lin, Torbjorn Nordling
# Email: eric.lee@nordlinglab.org, tn@nordron.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Initialize the variables
git_dir = ""
author_list = []
participant_list = []

# The extension to exclude from git log with the format of ... ':(exclude)*.xxx'
additional = " -- . ':(exclude)*.ilg' ':(exclude)*.ind' ':(exclude)*.ist' ':(exclude)*.lof' ':(exclude)*.log' ':(exclude)*.lot' ':(exclude)*.maf' ':(exclude)*.mtc' ':(exclude)*.mtc1' ':(exclude)*.out' ':(exclude)*.synctex.gz' ':(exclude)*.toc' ':(exclude)*.aux' ':(exclude)*.gz' ':(exclude)*.bbl' ':(exclude)*.blg' ':(exclude)Code/Django/src/SearchDB/calculators/*' ':(exclude)Code/Django/src/SearchDB/common.py' ':(exclude)Code/Django/src/SearchDB/textanalyzer.py'"

# Avoid escape sequence that makes shell unable to find git log
Escape = ['\\a','\\b','\\f','\\r','\\v']

# Threshold for the words in invalid commits
threshold = 3000

# The index of the highest score to use, use 0 for the maximum
highest_score = 0 

# The weighting of [commits, lines_inserted, lines_deleted, words_inserted, words_deleted]
wt = [0.3, 0.2, 0.15, 0.2, 0.15]

# Import packages
import subprocess
import datetime
import sys
import math
import os
from Participant import Participant

# Print out current participants
def print_Participants(arr):
	print(">> Participants: ")
	for i in range(len(arr)):
			print("%3d  %d  %-20s"%(i, arr[i].semester, arr[i].participant))
	print("")

# Print out the authors which is not assigned to the participants in git log
def print_Authors(arr):
	print(">> Authors Remain: ")
	for i in range(len(arr)):
                        print("%3d  %-50s\t%s"%(i, str(arr[i].author), str(arr[i].commits)))
        print("")

# Remove the last item in a list
def remove_last(arr):
	length = len(arr)
	arr = arr[:length-1]
	return arr

# Get data from cmd command
def getdata(cmd):
	y = cmd[0].decode('string_escape')
	y = y.replace("\\", "\\\\")
	p = subprocess.Popen(y, stdout=subprocess.PIPE, shell=True)
	processes = [p]
	for x in cmd[1:]:
		p = subprocess.Popen(x, stdin=p.stdout, stdout=subprocess.PIPE, shell=True)
        	processes.append(p)
	#for process in processes:
	#	process.wait()
	output = p.communicate()[0]
	return  remove_last(output.split("\n"))

# Get git repository
def getgitrep():
	if len(sys.argv) < 2:
		return False
	global git_dir
	git_dir = sys.argv[1]
	os.chdir(git_dir.split('.git')[0])
	return True

# Pull the latest version in git
def updaterep():
	print("\nUpdating git repository...")
	p = subprocess.Popen("git --git-dir=%s pull origin"%(git_dir), shell=True)
	p.wait()
	print("")

# Get the information about the lines inserted and deleted
def get_lines_data():
	for i in range(len(author_list)):
                cmd=["git --git-dir=%s log --all --numstat --author='%s'%s"%(git_dir, author_list[i].author[0], additional), \
			'grep "^[0-9]"', "awk '{inserted+=$1;deleted+=$2} END {print inserted,deleted}'"]
                (ins, dlt)=getdata(cmd)[0].split(" ")
		try:
			author_list[i].add_lines(int(ins), int(dlt))
		except:
				author_list[i].add_lines(0,0)

# Get the information about the words inserted and deleted
def get_words_data():
	for i in range(len(author_list)):
                cmd1 = ["git --git-dir=%s log -p --all --word-diff=porcelain --author='%s'%s"%(git_dir, author_list[i].author[0], additional), \
			'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                cmd2 = ["git --git-dir=%s log -p --all --word-diff=porcelain --author='%s'%s"%(git_dir, author_list[i].author[0], additional), \
			'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
		author_list[i].add_words(int(getdata(cmd1)[0]), int(getdata(cmd2)[0]))

# Get the authors in git history
def get_authors():
	count = 0
	cmd = ["git --git-dir=%s shortlog -sne --all HEAD"%(git_dir), "/usr/bin/awk 'BEGIN{FS=\"\t\"}{print $2,$1}'"]
        author_commits = getdata(cmd)
	print("Getting author information...")
        for x in author_commits:
                (aut, com) = x.split("    ")
		# aut = aut.split(" <")[0]
		author = Participant()
		author.add_commits(aut, int(com))
		author_list.append(author)
	get_lines_data()
	get_words_data()
	for x in author_list:
		count += 1
		print("%3d  %-55s\t%d\t%d\t%d\t%d"%(count, str(x.author[0]), x.lines_inserted[0], x.lines_deleted[0], x.words_inserted[0], x.words_deleted[0]))
	print("")

# Remove fake and invalid commits
def remove_fake_commits():
	count = 0
	cmd1 = ["git --git-dir=%s log --all%s"%(git_dir, additional), "grep '^commit '"]		
	f = getdata(cmd1)
	for i, c in enumerate(f):
                c = c.replace("\r","")
                c = c.replace(" ","")
                (a, b) = c.split("commit")
                f[i] = b

	script_dir = os.path.dirname(__file__)
        relative_path = "fake_commits.txt"
        absolute_path = os.path.join(script_dir, relative_path)
	cmd2 = ["grep 'commits/' "+absolute_path]
        g = getdata(cmd2)
        for i, c in enumerate(g):
                c = c.replace("\r","")
                c = c.replace(" ","")
		(a, b) = c.split("commits/")
		g[i] = b

	print("Checking fake commits...")
	for commit in f:
		cmd_author = ["git --git-dir=%s log %s -n 1%s"%(git_dir, commit, additional), "grep 'Author:'"]
                cmd_del_word = ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1%s"%(git_dir, commit, additional), \
                        'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                cmd_ins_word = ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1%s"%(git_dir,commit, additional), \
                        'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                cmd_line = ["git --git-dir=%s log --numstat %s -n 1%s"%(git_dir, commit, additional), \
                        'grep "^[0-9]"', "awk '{inserted+=$1;deleted+=$2} END {print inserted,deleted}'"]
		ins_word = int(getdata(cmd_ins_word)[0])
		del_word = int(getdata(cmd_del_word)[0])
		if commit in g or ins_word > threshold or del_word > threshold:
			count += 1
                	(y, z) = getdata(cmd_line)[0].split(" ")
                	(x, aut) = getdata(cmd_author)[0].split("Author: ")
                	# aut = aut.split(" <")[0]
			if commit in g:
				for author in author_list:
					if author.author == [aut]:
						author.remove_commit(1, 0, int(y), int(z), ins_word, del_word)
				print("%3d  %s  %-55s\t%d\t%d\t%d\t%d\t(FAKE)"%(count, str(commit), str(aut), int(y), int(z), ins_word, del_word))
			else:
				for author in author_list:
                	                if author.author == [aut]:
        	                                author.remove_commit(0, 1, int(y), int(z), ins_word, del_word)
	                        print("%3d  %s  %-55s\t%d\t%d\t%d\t%d"%(count, str(commit), str(aut), int(y), int(z), ins_word, del_word))
	print("Total fake commits: %d/%d\n"%(count, len(f)))

# Create participants
def create_participant(semester, participant_name, author_name):
	participant = Participant()
	participant.set_participant(participant_name)
	participant.set_semester(semester)
	for name in author_name:
		for author in author_list:
			if author.author == name:
				participant += author
				author_list.remove(author)
	participant_list.append(participant)
	# participant.print_info()

# Match participants to authors with the form of create_participant(year, 'participant', [author1, author2, ...])
def match_participants():
	create_participant(2015, 'Torbj\xc3\xb6rn Nordling', [['Torbj\xc3\xb6rn Nordling <tn@nordron.com>'], ['Torbj\xc3\xb6rn Nordling <tn@kth.se>']])
	create_participant(2016, 'Jacky Wu', [['Jacky Wu <Jacky@youande-MacBook-Pro.local>'], ['Yu-An Wu <jackywugogo@gmail.com>']])
	create_participant(2016, 'Eric Lee', [['Eric Lee <crazyeric890119@gmail.com>']])
	create_participant(2016, 'Eric Chang', [['Yu-Kai <ehero80425@gmail.com>'], ['Eric Chang <ehero80425@gmail.com>']])
	create_participant(2016, 'Karthik Mani', [['KARTHIK <mani.karthick.181190@gmail.com>']])
	create_participant(2016, 'U-Cheng Chen', [['UCheng Chen <pt10026@gmail.com>']])
	create_participant(2016, 'Dexter Chen', [['DexterChen <owesdexter2011@gmail.com>'], ['unknown <geminielf9@gmail.com>'], \
		['unknown <you@example.com>'], ['Dexter Chen <owesdexter2011@gmail.com>']])
	create_participant(2016, 'Kevin Lo', [['Kevin Lo <owl3808@gmail.com>']])
	create_participant(2016, 'I-Chieh Lin', [['l0989553696 <l0989553696@gmail.com>']])
	create_participant(2016, 'Chinweze', [['Chinweze <chinwezeubadigha@gmail.com>'], ['chinweze <chinwezeubadigha@gmail.com>']])
	create_participant(2016, 'Jones', [['JSGY <ndslgood@hotmail.com>']])
	create_participant(2016, 'Ray', [['leoc0426 <emanual0426@gmail.com>']])
	create_participant(2016, 'Henry Peng', [['Henry-Peng <kkvvy12@gmail.com>'], ['Henry <kkvvy12@gmail.com>']])
	create_participant(2016, 'Piyarul Hoque', [['Piyarul Hoque <piyarulhoque1993@gmail.com>'], ['Piyarul <piyarulhoque1993@gmail.com.com>'], \
		['Piyarul1993 <piyarulhoque1993@gmail.com>'], ['Piyarul <piyarulhoque1993@gmail.com>']])
	create_participant(2016, 'Tim Hsia', [['Feng Chun Hsia <tim.hsia@nordlinglab.org>'], ['FengChunHsia <tim.hsia@nordlinglab.org>']])
	create_participant(2016, 'Kenny Hsu', [['Kenny Hsu <tei1004@yahoo.com.tw>'], ['Kenny Hsu <teii1004@yahoo.com.tw>']])
	create_participant(2016, 'Tan Phat', [['TPhat <geminielf9@gmail.com>'], ['Lam Tan Phat <geminielf9@gmail.com>'], ['Tan Phat <geminielf@gmail.com>']])
	create_participant(2016, 'Jim Lan', [['Jim_Lan <jb0929n@gmail.com>'], ['Your NameJim_Lan <jb0929n@gmail.com>']])
	create_participant(2016, 'Hoang Tan', [['HoangTan <lopcatia@gmail.com>'], ['tony <lopcatia@gmail.com>']])
	create_participant(2016, 'Wei', [['Wei <4A02C014@stust.edu.tw>'], ['4A02C014 <4A02C014@stust.edu.tw>'], \
		['\xe5\x93\xb2\xe5\x81\x89 \xe5\xbc\xb5 <4a02c014@stust.edu.tw>']])
	create_participant(2016, 'Bernie Huang', [['Bernie Huang <bernie6430@gmail.com>'], ['Bo Han Huang <bernie6430@gmail.com>']])
	create_participant(2016, 'Rahul Aditya', [['Rahul <adi.rahul171294@gmail.com>']])
	create_participant(2016, 'Yu-Sin Lin', [['kurumalin <pallacanestro159@gmail.com>']])
	create_participant(2016, 'Elison Liu', [['Liucempc <liucempc@hotmail.com>'], ['Elison Liu <liucempc@hotmail.com>']])
	create_participant(2017, 'Lewis Hsu', [['Huan-wei Hsu <qqps4487@gmail.com>'], ['HuanWeiHsu <qqps4487@gmail.com>']])
	create_participant(2017, 'Dickson Lee', [['ds lee <dickson.lee@nordlinglab.org>'], ['Dickson Lee <dickson.lee@nordlinglab.org>'], ['dickson lee <dickson.lee@nordlinglab.org>']])
	create_participant(2017, 'Rain Wu', [['Rain Wu <Rain.Wu@nordlinglab.org>']])
	create_participant(2017, 'Sareddy Reddy', [['sareddy17 <sareddy.kullaireddy3173@gmail.com>'], ['sareddy kullai reddy <sareddy.kullaireddy3173@gmail.com>']])
	create_participant(2017, 'Paul Lin', [['linpohsien <a4839500@gmail.com>']])
	create_participant(2017, 'Van Tam Ngo', [['Tamnv14 <ngovantam.haui@gmail.com>'], ['me813_Tam-PC\\me813_Tam <ngovantam.haui@gmail.com>'], ['van tam ngo <ngovantam.haui@gmail.com>']])
	create_participant(2018, 'Alexander Heimann', [['Alexander Heimann <alexander.heimann@nordlinglab.org>'],['Alexander <alexander.heimann@nordlinglab.org>']])
	create_participant(2018, 'Michael Huang', [['N16041391\\user <112231tn@gmail.com>'],['MichaelHuang <112231tn@gmail.com>']])
	create_participant(2018, 'Justin Lin', [['justinlin <justin.lin@nordlinglab.org>'],['MSI\\ap932 <justin.lin@nordlinglab.org>'],['Justin lin <justin.lin@nordlinglab.org>']])
	create_participant(2018, 'Jose Chang', [['unknown <jose.chang@nordlinglab.org>']])
	create_participant(2018, 'Tria', [['tria <tria@nordlinglab.org>']])
	create_participant(2018, 'Cathy Huang', [['DESKTOP-U5H577O\\\xe7\x90\xb3\xe7\x9a\x93 <cathy840101@gmail.com>'],['huang cathy <cathy840101@gmail.com>']])
	create_participant(2018, 'Ray Chen', [['Ray <raiya5316@hotmail.com>'],['Ray Chen <raiya5316@hotmail.com>'],['RayChen <raiya5316@hotmail.com>']])
	create_participant(2018, 'Ralph Nasara', [['rnvnasara <rnvnasara@gmail.com>']])
	create_participant(2018, 'Cindy Hsu', [['cindyhsu <d2570841@gmail.com>']])
	create_participant(2018, 'Kevin Liao', [['Kevin Liao <abigger0515@gmail.com>']])
	create_participant(2018, 'Paul Tsai', [['Paul Tsai <tahsdj@gmail.com>']])
	create_participant(2018, 'Alan Yu', [[r'DESKTOP-E83N3GJ\alan11223a <alan.yu@nordlinglab.org>'],['alanyu <alan.yu@nordlinglab.org>']])
	create_participant(2018, 'Nick Chen', [['Nick Chen <fasionout@gmail.com>'],['Pin-Tsung Chen <nick.chen@nordlinglab.org>']])
	create_participant(2018, 'Oliver Lai', [['Oliver <oliverlai134@gmail.com>']])
	create_participant(2018, 'Firhan Huzaefa', [['Firhan <firhanh@gmail.com>']])
	create_participant(2018, 'Ben Hsu', [['WEI\\ben83 <ben831013@gmail.com>'],['Ben Hsu <ben831013@gmail.com>']])
	create_participant(2018, 'Annas Fauzy', [['annasfauzy10 <annasfauzy10@gmail.com>'],['Annas Fauzy <annasfauzy@gail.com>'],['DESKTOP-5MEDO2H\\Annas Fauzy <annasfauzy10@gmail.com>']])
	create_participant(2018, 'Eric Tu', [['\xe6\x9d\x9c\xe6\x94\xbf\xe6\x9d\xb0 <chengjdu@gmail.com>'],['EricDu <chengjdu@gmail.com>']])
	create_participant(2018, 'Sunny Tseng', [['Sunny <sunny567886@gmail.com>'],['SunnyChing <sunny567886@gmail.com>']])

	print_Participants(participant_list)	
	print_Authors(author_list)

# Calculate the score of each participant
def score_func(arr, val):
	arr_sum = sum(arr)
	if arr_sum > 0:
		score = math.log10(arr_sum/float(val))
	else:
		score = -10
	return score

# Calculate git score with git log
def generate_git_score():
	index = 0
	prof = []
	commits = []
	lines_inserted = []
	lines_deleted = []
	words_inserted = []
	words_deleted = []
	for participant in participant_list:
		if participant.participant == 'Torbj\xc3\xb6rn Nordling': 
			prof = [sum(participant.commits), sum(participant.lines_inserted), sum(participant.lines_deleted), \
                                sum(participant.words_inserted), sum(participant.words_deleted)]
	for participant in participant_list:
		if not participant.participant == 'Torbj\xc3\xb6rn Nordling':
			commits.append(score_func(participant.commits, prof[0]))
			lines_inserted.append(score_func(participant.lines_inserted, prof[1]))
			lines_deleted.append(score_func(participant.lines_deleted, prof[2]))
			words_inserted.append(score_func(participant.words_inserted, prof[3]))
			words_deleted.append(score_func(participant.words_deleted, prof[4]))
	sorted_list = [sorted(commits, reverse=True), sorted(lines_inserted, reverse=True), sorted(lines_deleted, reverse=True), sorted(words_inserted, reverse=True), sorted(words_deleted, reverse=True)]
	maximum = [i[highest_score] for i in sorted_list]
	#maximum = [score_func([88], prof[0]), score_func([2104], prof[1]), score_func([1843], prof[2]), score_func([16609], prof[3]), score_func([6115], prof[4])]
	for i in range(len(participant_list)-1):
		if commits[i] <= maximum[0]:
			commits[i] = 70+30*(commits[i]/float(maximum[0]))
		else:
			commits[i] = 100
		if lines_inserted[i] <= maximum[1]:
			lines_inserted[i] = 70+30*(lines_inserted[i]/float(maximum[1]))
                else:
                        lines_inserted[i] = 100
		if lines_deleted[i] <= maximum[2]:
			lines_deleted[i] = 70+30*(lines_deleted[i]/float(maximum[2]))
                else:
			lines_deleted[i] = 100
		if words_inserted[i] <= maximum[3]:
			words_inserted[i] = 70+30*(words_inserted[i]/float(maximum[3]))
                else:
			words_inserted[i] = 100
		if words_deleted[i] <= maximum[4]:
			words_deleted[i] = 70+30*(words_deleted[i]/float(maximum[4]))
                else:
			words_deleted[i] = 100
	for participant in participant_list:
                if not participant.participant == 'Torbj\xc3\xb6rn Nordling':
			participant.set_git_score(commits[index]*wt[0]+lines_inserted[index]*wt[1]+lines_deleted[index]*wt[2]+ \
				words_inserted[index]*wt[3]+words_deleted[index]*wt[4])
			index += 1
		else:
			participant.set_git_score(70)

# Generate the statistics
def generate_statistics():
	if not getgitrep():
		print("Lack of git repository parameter!!")
		sys.exit()
	updaterep()
	get_authors()
	remove_fake_commits()
	match_participants()
	generate_git_score()

# Create the html file
def create_html():
	print("Generating HTML file...")
	script_dir = os.path.dirname(__file__)
	relative_path = "html/index.html"
	absolute_path = os.path.join(script_dir, relative_path)
	recent_semester = max([participant.semester for participant in participant_list])
	with open(absolute_path, "w") as f:
		format = '%Y-%m-%d %H:%M:%S'
		f.write('<!DOCTYPE html>\n')
		f.write('<html>\n')
		f.write('  <head>\n')
		f.write('    <meta charset="UTF-8">\n')
		f.write('    <title>Statistics</title>\n')
		f.write('    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">\n')
		f.write('    <link href="css/table.css" rel="stylesheet" type="text/css" />\n')
		f.write('    <link href="css/container.css" rel="stylesheet" type="text/css" />\n')
		f.write('		 <link href="/favicon.png" rel="shortcut icon" type="image/png" />\n')
		f.write('    <script type="text/javascript" src="score.js"></script>\n')
		f.write('    <script type="text/javascript" src="jquery-latest.js"></script>\n')
		f.write('    <script type="text/javascript" src="jquery.tablesorter.js"></script>\n')
		f.write('    <script type="text/javascript">\n')
		f.write('    $(document).ready(function() {\n')
		f.write('      $(\'#statistic\').tablesorter();\n')
		f.write('      $(\'#score\').tablesorter();\n')
		f.write('      $(\'td:contains("2015")\').parent().children().css(\'background-color\', \'rgb(225, 225, 225)\');\n')
		f.write('      var years = $(\'td:nth-child(2)\');\n')
		f.write('      $.each(years, function(idx, td_year){\n')
		f.write('        var td_year = $(td_year);\n')
		f.write('        if(td_year.text() != %d) return;\n'%(recent_semester))
		f.write('        \n')
		f.write('        var tr = td_year.parent();\n')
		f.write('        var score = tr.find(\'td:last-child\').text();\n')
		f.write('        if (score >= 70){\n')
		f.write('          tr.children().css(\'background-color\', \'rgb(220, 245, 220)\');\n')
		f.write('        }else{\n')
		f.write('          tr.children().css(\'background-color\', \'rgb(245, 220, 220)\');\n')
		f.write('        }\n')
		f.write('      });\n')
		f.write('    });\n')
		f.write('    </script>\n')
		f.write('  </head>\n')
		f.write('  <body>\n')
		f.write('    <div class="container" id="header">\n')
		f.write('      <h1>Scientific Information Gathering and Processing for Engineering Research 2018</h1>\n')
		f.write('      <p>Accessed at %s from git repository.</p>\n' \
			%(datetime.datetime.now().strftime(format)))
		f.write('    </div>\n')
		f.write('\n')
		f.write('    <div class="container">\n')
		f.write('      <h2>Statistics of the Commits on Bitbucket</h2>\n')
		f.write('      <table cellspacing="2" id="statistic" class="tablesorter">\n')
		f.write('        <thead>\n')
		f.write('        <tr>\n')
		f.write('          <th>Authors</th>\n')
		f.write('          <th>Semester</th>\n')
		f.write('          <th>Fake Commits</th>\n')
		f.write('          <th>Invalid Commits</th>\n')
		f.write('          <th>Valid Commits</th>\n')
		f.write('          <th>Line Inserted</th>\n')
		f.write('          <th>Line Deleted</th>\n')
		f.write('          <th>Word Inserted</th>\n')
		f.write('          <th>Word Deleted</th>\n')
		f.write('          <th>Total GIT Score</th>\n')
		f.write('        </tr>\n')
		f.write('        </thead>\n')
		f.write('        <tbody>\n')

		for participant in participant_list:
			f.write('          <tr>\n')
			f.write('            <td>%s</td>\n'%(participant.participant))
			f.write('            <td>%d</td>\n'%(participant.semester))
			f.write('            <td>%d</td>\n'%(participant.fake_commits))
			f.write('            <td>%d</td>\n'%(participant.large_commits))
			f.write('            <td>%d</td>\n'%(sum(participant.commits)))
			f.write('            <td>%d</td>\n'%(sum(participant.lines_inserted)))
			f.write('            <td>%d</td>\n'%(sum(participant.lines_deleted)))
			f.write('            <td>%d</td>\n'%(sum(participant.words_inserted)))
			f.write('            <td>%d</td>\n'%(sum(participant.words_deleted)))
			f.write('            <td>%.2f</td>\n'%(participant.git_score))
			f.write('          </tr>\n')

		f.write('        </tbody>\n')
		f.write('      </table>\n')
		if len(author_list) == 0:
			f.write('      <p id="total">&#10004; Total authors: %d </p>\n'%(len(participant_list)))
		else:
			f.write('      <p id="total">&#10006; Total authors: %d </p>\n'%(len(participant_list)))
		f.write('      <p>Note that merges won\'t give unfair points.</p>\n')
		f.write('    </div>\n')
		f.write('\n')
#		f.write('    <div class="container" id="score_div">\n')
#		f.write('      <h2>Total Score for the Course</h2>\n')
#		f.write('      <button class="btn" onclick="calculate()">Calculate Total Score</button>\n')
#		f.write('      <table cellspacing="2" id="score" class="tablesorter">\n')
#		f.write('        <thead>\n')
#		f.write('          <tr>\n')
#		f.write('            <th>Participants</th>\n')
#		f.write('            <th>Attendace at lecture</th>\n')
#		f.write('            <th>Attendance at daily scrum</th>\n')
#		f.write('            <th>GIT Score</th>\n')
#		f.write('            <th>Oral presentation</th>\n')
#		f.write('            <th>Quiz Score</th>\n')
#		f.write('            <th>Report Score</th>\n')
#		f.write('            <th>Total Score</th>\n')
#		f.write('          </tr>\n')
#		f.write('        </thead>\n')
#		f.write('        <tbody>\n')
#		
#		for participant in participant_list:
#                        f.write('          <tr>\n')
#                        f.write('            <td>%s</td>\n'%(participant.participant))
#                        f.write('            <td>%d</td>\n'%(participant.semester))
#                        f.write('            <td>%d</td>\n'%(0))
#                        f.write('            <td>%d</td>\n'%(0))
#                        f.write('            <td>%d</td>\n'%(0))
#                        f.write('            <td>%d</td>\n'%(0))
#			f.write('            <td>\n')
#			f.write('              <input type="text" value="85" onkeypress="return isNumberKey(event)" maxlength="3">\n')
#			f.write('            </td>\n')
#                        f.write('            <td>%d</td>\n'%(0))
#                        f.write('          </tr>\n')
#
#		f.write('        </tbody>\n')
#		f.write('      </table>\n')
#		f.write('    </div>\n')
		f.write('\n')
		f.write('  </body>\n')
		f.write('</html>\n')	

	print("")

generate_statistics()
create_html()
