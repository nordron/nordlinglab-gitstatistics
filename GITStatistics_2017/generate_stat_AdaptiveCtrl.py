#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2017 Nordron AB
# Authors: Wei-Yu Lee, Yu-Sin Lin, Torbjorn Nordling
# Email: eric.lee@nordlinglab.org, tn@nordron.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Initialize the variables
git_dir = ""
html_dir = ""
json_dir = ""
author_list = []
participant_list = []
file_list = []

# The extension to exclude from git log with the format of ... ':(exclude)*.xxx'
# additional = " -- . ':(exclude)*.ilg' ':(exclude)*.ind' ':(exclude)*.ist' ':(exclude)*.lof' ':(exclude)*.log' ':(exclude)*.lot' ':(exclude)*.maf' ':(exclude)*.mtc' ':(exclude)*.mtc1' ':(exclude)*.out' ':(exclude)*.synctex.gz' ':(exclude)*.toc' ':(exclude)*.aux' ':(exclude)*.gz' ':(exclude)*.bbl' ':(exclude)*.blg'"
additional = " -- ':(exclude)*.mat' *.m *.py *.tex"
additional1 = " ':(exclude)Presentation_NordlingLab_template/*' ':(exclude)Lecture_notes/*' ':(exclude)Nordlinglab template/*' ':(exclude)*beamer.tex' ':(exclude)*NordlingLab*' ':(exclude)*logo.pdf' "

# The extensions to include
included = ['.m', '.py', '.tex']
fig = ['.pdf', '.png', '.eps']

# Threshold for the words in invalid commits
threshold = 3000

# The index of the highest score to use, use 0 for the maximum
highest_score = 0

# The weighting of [commits, lines_inserted, lines_deleted, words_inserted, words_deleted]
wt = [0.3, 0.2, 0.15, 0.2, 0.15]

# Import packages
import subprocess
import datetime
import sys
import math
import os
import json
from collections import OrderedDict
from operator import itemgetter
from Participant import Participant, Files

# Print out current participants


def print_Participants(arr):
    print(">> Participants: ")
    for i in range(len(arr)):
        print("%3d  %d  %-20s" % (i, arr[i].semester, arr[i].participant))
    print("")

# Print out the authors which is not assigned to the participants in git log


def print_Authors(arr):
    print(">> Authors Remain: ")
    for i in range(len(arr)):
        print("%3d  %-50s\t%s" % (i, str(arr[i].author), str(arr[i].commits)))
    print("")

# Remove the last item in a list


def remove_last(arr):
    length = len(arr)
    arr = arr[:length-1]
    return arr

# Get data from cmd command


def getdata(cmd):
    y = cmd[0].decode('string_escape')
    y = y.replace("\\", "\\\\")
    p = subprocess.Popen(y, stdout=subprocess.PIPE, shell=True)
    processes = [p]
    for x in cmd[1:]:
        p = subprocess.Popen(x, stdin=p.stdout, stdout=subprocess.PIPE, shell=True)
        processes.append(p)
    for process in processes:
        process.wait()
    output = p.communicate()[0]
    return remove_last(output.split("\n"))

# Get git repository


def getgitrep():
    if len(sys.argv) < 3:
        return False
    global git_dir, html_dir, json_dir
    git_dir = sys.argv[1]
    html_dir = sys.argv[2]
    json_dir = sys.argv[3]
    os.chdir(git_dir.split('.git')[0])
    return True

# Pull the latest version in git


def updaterep():
    print("\nUpdating git repository...")
    p = subprocess.Popen("git --git-dir=%s pull origin" % (git_dir), shell=True)
    p.wait()
    print("")

# Get the information about the lines inserted and deleted


def get_lines_data():
    for i in range(len(author_list)):
        cmd = ["git --git-dir=%s log --numstat --author='%s'%s" % (git_dir, author_list[i].author[0], additional+additional1),
            'grep "^[0-9]"', "awk '{inserted+=$1;deleted+=$2} END {print inserted,deleted}'"]
        (ins, dlt) = getdata(cmd)[0].split(" ")
        try:
            author_list[i].add_lines(int(ins), int(dlt))
        except:
            author_list[i].add_lines(0, 0)

# Get the information about the words inserted and deleted


def get_words_data():
    for i in range(len(author_list)):
        cmd1 = ["git --git-dir=%s log -p --word-diff=porcelain --author='%s'%s" % (git_dir, author_list[i].author[0], additional+additional1),
            'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
        cmd2 = ["git --git-dir=%s log -p --word-diff=porcelain --author='%s'%s" % (git_dir, author_list[i].author[0], additional+additional1),
            'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
        author_list[i].add_words(int(getdata(cmd1)[0]), int(getdata(cmd2)[0]))

# Get the authors in git history


def get_authors():
    count = 0
    cmd = ["git --git-dir=%s shortlog -sne HEAD" % (git_dir), "/usr/bin/awk 'BEGIN{FS=\"\t\"}{print $2,$1}'"]
    author_commits = getdata(cmd)
    print("Getting author information...")
    for x in author_commits:
        (aut, com) = x.split("    ")
    # aut = aut.split(" <")[0]
        author = Participant()
        author.add_commits(aut, int(com))
        author_list.append(author)
    get_lines_data()
    get_words_data()
    for x in author_list:
        count += 1
        print(
            "%3d  %-55s\t%d\t%d\t%d\t%d" %
            (count, str(x.author[0]),
             x.lines_inserted[0],
             x.lines_deleted[0],
             x.words_inserted[0],
             x.words_deleted[0]))
    print("")
    for i in author_list:
        if [j for j in i.author if 'Huan-wei Hsu' in j]:
            author_list.remove(i)
            break

# Get the data of each file


def get_filename_n_owner(ext, figure=False):
    file_and_owner = []
    relpath = ''
    owner = ''
    ins_lines = 0
    del_lines = 0
    # git dir without .git/
    git_dir_wogit = git_dir.split('.git/')[0]
    # Find all the files that match the extensions in the repository
    if not figure:
        for i in ext:
            cmd = "git --git-dir=%s log --numstat -- ':(exclude)*.mat' *%s %s | grep '%s$'" % (git_dir, i, additional1, i)
            # cmd = 'find %s -name *%s'%(git_dir_wogit, i)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
            tmp = p.communicate()[0]
            tmp = [i.split('\n')[0] for i in tmp.split('\t') if '\n' in i]
            tmp = list(OrderedDict.fromkeys(tmp))
            # For each file, find the owner, i.e. first commit
            for j in tmp:
                # Relative path is needed
                cmd = 'git --git-dir=%s log --numstat -- %s %s | tail | grep -A1 "Author"' % (git_dir, j, additional1)
                cmd_commit = 'git --git-dir=%s log --numstat -- %s %s | grep "%s"' % (git_dir, j, additional1, j)
                cmd_wordsins = ["git --git-dir=%s log -p --word-diff=porcelain -- %s %s" % (git_dir, j, additional1),
                                'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                cmd_wordsdel = ["git --git-dir=%s log -p --word-diff=porcelain -- %s %s" % (git_dir, j, additional1),
                                'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                words_ins = int(getdata(cmd_wordsins)[0])
                words_del = int(getdata(cmd_wordsdel)[0])
                commits = remove_last(subprocess.Popen(cmd_commit, stdout=subprocess.PIPE,
                                    shell=True).communicate()[0].split('\n'))
                for k in commits:
                    try:
                        ins_lines += int(k.split('\t')[0])
                    except:
                        ins_lines += 0
                    try:
                        del_lines += int(k.split('\t')[1])
                    except:
                        del_lines += 0
                p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
                tmp = p.communicate()[0].split('\n')
                # Fetch the needed information according to the format of git log
                try:
                    owner = tmp[0].replace('Author: ', '')
                    year = int(tmp[1].split(' ')[-2])
                    file_and_owner.append([j, owner, year, len(commits), ins_lines, del_lines, words_ins, words_del])
                except:
                    pass
                ins_lines = 0
                del_lines = 0
        create_file(file_and_owner)
    else:
        for i in ext:
            cmd = "git --git-dir=%s log --numstat -- *%s %s | grep '%s$'" % (git_dir, i, additional1, i)
            p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
            tmp = p.communicate()[0]
            tmp = [i.split('\n')[0] for i in tmp.split('\t') if '\n' in i]
            tmp = list(OrderedDict.fromkeys(tmp))
            filename = [i.split('/')[-1] for i in tmp]

            for j in tmp:
                if '(' in j:
                    j = j.replace('(','\(')
                if ')' in j:
                    j = j.replace(')','\)')
                cmd = 'git --git-dir=%s log --numstat -- %s %s | tail | grep -A1 "Author"' % (git_dir, j, additional1)
                cmd_author = ['git --git-dir=%s log --numstat -- %s %s | grep "Author"' % (git_dir, j, additional1)]
                cmd_commit = 'git --git-dir=%s log --numstat -- %s %s | grep "%s"' % (git_dir, j, additional1, j)
                commits = remove_last(subprocess.Popen(cmd_commit, stdout=subprocess.PIPE,
                                        shell=True).communicate()[0].split('\n'))
                p = subprocess.Popen(cmd, stdout=subprocess.PIPE, shell=True)
                author_string = p.communicate()[0].split('\n')
                try:
                    owner = author_string[0].replace('Author: ', '')
                    year = int(author_string[1].split(' ')[-2])
                    file_and_owner.append([j, owner, year, len(commits), 0, 0, 50*(len(commits)+1), 0])
                except:
                    pass
                for k in getdata(cmd_author):
                    author_info= k.replace('Author: ', '')
                    author = [x for x in author_list if x.author == [author_info]]
                    if author_info == owner:
                        for l in author_list:
                            if author_info in l.author:
                                l.add_words(100,0)
                                break
                    else:
                        for l in author_list:
                            if author_info in l.author:
                                l.add_words(50,0)
                                break
        create_file(file_and_owner)

# Remove fake and invalid commits


def remove_fake_commits():
    count= 0
    cmd1= ["git --git-dir=%s log --all%s" % (git_dir, additional+additional1), "grep '^commit '"]
    f= getdata(cmd1)
    for i, c in enumerate(f):
        c= c.replace("\r", "")
        c= c.replace(" ", "")
        (a, b)= c.split("commit")
        f[i]= b

    script_dir= os.path.dirname(__file__)
    relative_path= "fake_commits_AdaptiveCtrl.txt"
    absolute_path= os.path.join(script_dir, relative_path)
    cmd2= ["grep 'commits/' "+absolute_path]
    g= getdata(cmd2)
    for i, c in enumerate(g):
        c= c.replace("\r", "")
        c= c.replace(" ", "")
        (a, b)= c.split("commits/")
        g[i]= b

    print("Checking fake commits...")
    for commit in f:
        cmd_author= ["git --git-dir=%s log %s -n 1%s" % (git_dir, commit, additional), "grep 'Author:'"]
        cmd_del_word= ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1%s" % (git_dir, commit, additional+additional1),
                        'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
        cmd_ins_word= ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1%s" % (git_dir, commit, additional+additional1),
                        'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
        cmd_line= ["git --git-dir=%s log --numstat %s -n 1%s" % (git_dir, commit, additional+additional1),
                        'grep "^[0-9]"', "awk '{inserted+=$1;deleted+=$2} END {print inserted,deleted}'"]
        ins_word= int(getdata(cmd_ins_word)[0])
        del_word= int(getdata(cmd_del_word)[0])
        if commit in g or ins_word > threshold or del_word > threshold:
            count += 1
            (y, z)= getdata(cmd_line)[0].split(" ")
            (x, aut)= getdata(cmd_author)[0].split("Author: ")
            # aut = aut.split(" <")[0]
            if commit in g:
                for author in author_list:
                    if author.author == [aut]:
                        author.remove_commit(1, 0, int(y), int(z), ins_word, del_word)
                print("%3d  %s  %-55s\t%d\t%d\t%d\t%d\t(FAKE)" %
                      (count, str(commit), str(aut), int(y), int(z), ins_word, del_word))
                cmd_file_line= ["git --git-dir=%s log --numstat %s -n 1%s" % (git_dir, commit, additional+additional1),
                        'grep "^[0-9]"']
                file_information= getdata(cmd_file_line)
                for info in file_information:
                    tmp_info= info.split('\t')
                    file_ins_line= tmp_info[0]
                    file_del_line= tmp_info[1]
                    file_name= tmp_info[2]
                    cmd_file_ins_word= ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1 -- %s" % (git_dir, commit, file_name),
                        'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                    cmd_file_del_word= ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1 -- %s" % (git_dir, commit, file_name),
                        'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                    fake_file_del_word= int(getdata(cmd_file_del_word)[0])
                    fake_file_ins_word= int(getdata(cmd_file_ins_word)[0])
                    for i in file_list:
                        if file_name is i.filename:
                            i.remove_commit(1, 0, file_ins_line, file_del_line, fake_file_ins_word, fake_file_del_word)
                            break
            else:
                for author in author_list:
                    if author.author == [aut]:
                        author.remove_commit(0, 1, int(y), int(z), ins_word, del_word)
                        print(
                            "%3d  %s  %-55s\t%d\t%d\t%d\t%d" %
                            (count, str(commit),
                             str(aut),
                             int(y),
                             int(z),
                             ins_word, del_word))
                cmd_file_line= ["git --git-dir=%s log --numstat %s -n 1%s" % (git_dir, commit, additional+additional1),
                        'grep "^[0-9]"']
                file_information= getdata(cmd_file_line)
                for info in file_information:
                    tmp_info= info.split('\t')
                    file_ins_line= tmp_info[0]
                    file_del_line= tmp_info[1]
                    file_name= tmp_info[2]
                    cmd_file_ins_word= ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1 %s" % (git_dir, commit, file_name),
                        'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                    cmd_file_del_word= ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1 %s" % (git_dir, commit, file_name),
                        'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                    fake_file_del_word= int(getdata(cmd_file_del_word)[0])
                    fake_file_ins_word= int(getdata(cmd_file_ins_word)[0])
                    for i in file_list:
                        if file_name is i.filename:
                            i.remove_commit(0, 1, file_ins_line, file_del_line, fake_file_ins_word, fake_file_del_word)
                            break
    print("Total fake commits: %d/%d\n" % (count, len(f)))

# Restore commits that were labeled as invalid


def recovery(filename):
    script_dir= os.path.dirname(__file__)
    filename= os.path.join(script_dir, filename)
    print('Recovering invalid commits:\n')
    with open(filename,'r') as f:
        recovered_commits = 0
        for i in f:
            info = i.split(' ')
            info[0] = info[0].split('/')[-1]    # Extract commit hash
            commit = info[0]
            count = 0
            commited_files = []
            lines_ins = []
            lines_del = []
            words_ins = []
            words_del = []
            while('\n' not in info[count]):
                count += 1
                commited_files.append(info[count])
                count += 1
                lines_ins.append(int(info[count]))
                count += 1
                lines_del.append(int(info[count]))
                count += 1
                words_ins.append(int(info[count]))
                count += 1
                words_del.append(int(info[count]))
            cmd_author= ["git --git-dir=%s log %s -n 1%s" % (git_dir, commit, additional), "grep 'Author:'"]
            (x, aut)= getdata(cmd_author)[0].split("Author: ")
            # aut = aut.split(" <")[0]
            for author in author_list:
                if author.author == [aut]:
                    author.restore_commit(sum(lines_ins),sum(lines_del),sum(words_ins),sum(words_del))
                    print('%s %s %d %d %d %d\n'%(commit,aut,sum(lines_ins),sum(lines_del),sum(words_ins),sum(words_del)))
                    break
            cmd_file_line= ["git --git-dir=%s log --numstat %s -n 1%s" % (git_dir, commit, additional+additional1),
                        'grep "^[0-9]"']
            file_information= getdata(cmd_file_line)
            for info in file_information:
                tmp_info= info.split('\t')
                file_ins_line= tmp_info[0]
                file_del_line= tmp_info[1]
                file_name= tmp_info[2]
                cmd_file_ins_word= ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1 -- %s" % (git_dir, commit, file_name),
                    'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                cmd_file_del_word= ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1 -- %s" % (git_dir, commit, file_name),
                    'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                file_del_word= int(getdata(cmd_file_del_word)[0])
                file_ins_word= int(getdata(cmd_file_ins_word)[0])
                if file_name not in commited_files:
                    for j in file_list:
                        if file_name is j.filename:
                            j.restore_commit(file_ins_line,file_del_line,file_ins_word,file_del_word)
                            break
                else:
                    for j in file_list:
                        try:
                            index = commited_files.index(j.filename)
                            j.restore_commit(file_ins_line-lines_ins[index],file_del_line-lines_del[index],file_ins_word-words_ins[index],file_del_word-words_del[index])
                        except:
                            continue
            recovered_commits += 1
        print('Total recovered commits: %d\n'%(recovered_commits))

# Create participants


def create_participant(semester, participant_name, author_name):
    participant= Participant()
    participant.set_participant(participant_name)
    participant.set_semester(semester)
    # One participant might have mutiple identities when committing
    for name in author_name:
        for author in author_list:
            if author.author == name:
                participant += author
                author_list.remove(author)
    participant_list.append(participant)
    # participant.print_info()

# Create file object


def create_file(file_and_owner):
        for i in file_and_owner:
                original= Files()
                original.set_filename(i[0])									# Filename
                original.set_participant(i[1])							# Set the owner of the file
                original.set_semester(i[2])
                original.add_commits(i[1], i[3])
                original.add_lines(i[4], i[5])
                original.add_words(i[6], i[7])
                file_list.append(original)

# Match participants to authors with the form of create_participant(year, 'participant', [author1, author2, ...])


def match_participants():
    create_participant(
        2018, 'Torbj\xc3\xb6rn Nordling',
        [['Torbj\xc3\xb6rn Nordling <tn@nordron.com>'],
         ['Torbj\xc3\xb6rn Nordling <tn@kth.se>']])
    create_participant(2018, 'Rain Wu', [['Rain Wu <Rain.Wu@nordlinglab.org>'], ['unknown <rain.wu@nordlinglab.org>']])
    create_participant(2018, 'Oliver Lai', [['Laipoyu <oliverlai134@gmail.com>'], ['oliver <oliverlai134@gmail.com>']])
    create_participant(2018, 'Jack Wang', [['Jack Wang <jack.wang@nordlinglab.org>'], [
                       'Jack Wang <JackWang@nordlinglab.org>']])
    create_participant(2018, 'Jose Chang', [['unknown <jose.chang@nordlinglab.org>']])
    create_participant(2018, 'David Li', [['unknown <cobras1597535@gmail.com>']])
    create_participant(
        2018, 'Peter Peng',
        [['DESKTOP-DS9CEOD\\Peter Peng <s12359e.hchs@gmail.com>'],
         ['Peter Peng <NungNung840321n@bitbucket.org>']])
    create_participant(
        2018, 'Firhan Huzaefa',
        [['DESKTOP-0OKUSIN\\Firhan <firhanh@gmail.com>'],
         ['DESKTOP-24NUB8B\\Firhan <firhanh@gmail.com>'],
         ['Firhan <firhanh@gmail.com>']])
    create_participant(2018, 'Shih-hsiu Chen', [['ShihHsiuChen <s8432208@gmail.com>']])
    create_participant(2018, 'Bob Chen', [['Bob_Chen <b840706b@gmail.com>']])
    create_participant(2018, 'Ivan Chou', [['DESKTOP-RNKTOKE\\Ivan Chou <ivan199701@hotmail.com>']])
    create_participant(2018, 'Tim Oris', [['Tim <timorisli@gmail.com>']])
    create_participant(2018, 'Tsen-chang Lin', [['Lin_Tsen_Chang <ominuby@hotmail.com>']])
    create_participant(2018, 'Michael Chua', [['MichaelJohnChua <michael840608@gmail.com>']])
    create_participant(
        2018, 'Van Tam Ngo',
        [['me813_Tam-PC\\me813_Tam <ngovantam.haui@gmail.com>'],
         ['Tam <ngovantam.haui@gmail.com>']])
    create_participant(2018, 'Tim Chen', [['Tim Chen <olddeadman@gmail.com>'], ['unknown <skywalkertim@hotmail.com>']])
    create_participant(2018, 'Yellow Huang', [['Archer <havediefire@gmail.com>'],['ASUS\\USER <havediefire@gmail.com>']])
    print_Participants(participant_list)
    print_Authors(author_list)

# Calculate the score of each participant


def score_func(arr, arr_fake, val):
    arr_sum= sum(arr) - 2*sum(arr_fake)
    if arr_sum > 0 and float(val) != 0:
        score= math.log10(arr_sum/float(val))
    else:
        score= -3
    return score

# Calculate git score with git log


def generate_git_score():
    index= 0
    prof= []
    commits= []
    lines_inserted= []
    lines_deleted= []
    words_inserted= []
    words_deleted= []
    for participant in participant_list:
        if participant.participant == 'Torbj\xc3\xb6rn Nordling':
            prof= [sum(participant.commits), sum(participant.lines_inserted), sum(participant.lines_deleted),
                                sum(participant.words_inserted), sum(participant.words_deleted)]
    for participant in participant_list:
        commits.append(score_func(participant.commits, [0], prof[0]))
        lines_inserted.append(score_func(participant.lines_inserted, participant.lines_inserted_fake, prof[1]))
        lines_deleted.append(score_func(participant.lines_deleted, participant.lines_deleted_fake, 200))
        words_inserted.append(score_func(participant.words_inserted, participant.words_inserted_fake, 3000))
        words_deleted.append(score_func(participant.words_deleted, participant.words_deleted_fake, 500))
    sorted_list= [
        sorted(commits, reverse=True),
        sorted(lines_inserted, reverse=True),
        sorted(lines_deleted, reverse=True),
        sorted(words_inserted, reverse=True),
        sorted(words_deleted, reverse=True)]
    maximum= [i[highest_score] for i in sorted_list]
    print(maximum)
    for i in range(len(participant_list)):
        if commits[i] <= maximum[0]:
                if commits[i] <= 0:
                        commits[i]= 70+30*commits[i]
                else:
                        commits[i]= 70+30*(commits[i]/float(maximum[0]))*(abs(maximum[0])/maximum[0])
        else:
            commits[i]= 100
        if lines_inserted[i] <= maximum[1]:
            lines_inserted[i]= 70+30*(lines_inserted[i]/float(maximum[1]))*(abs(maximum[1])/maximum[1])
        else:
            lines_inserted[i]= 100
        if lines_deleted[i] <= maximum[2]:
            lines_deleted[i]= 70+30*(lines_deleted[i]/float(maximum[2]))*(abs(maximum[2])/maximum[2])
        else:
            lines_deleted[i]= 100
        if words_inserted[i] <= maximum[3]:
                if words_inserted[i] <= 0:
                        words_inserted[i]= 70+30*words_inserted[i]
                else:
                        words_inserted[i]= 70+30*(words_inserted[i]/float(maximum[3]))*(abs(maximum[3])/maximum[3])
#                print("Words inserted: %g"%(words_inserted[i]))
        else:
            words_inserted[i]= 100
        if words_deleted[i] <= maximum[4]:
                if words_deleted[i] <= 0:
                        words_deleted[i]= 70+30*words_deleted[i]
                else:
                        words_deleted[i]= 70+30*(words_deleted[i]/float(maximum[4]))*(abs(maximum[4])/maximum[4])
#                print("Words deleted: %g"%(words_deleted[i]))
        else:
            words_deleted[i]= 100
    for participant in participant_list:
        participant.set_git_score(words_inserted[index]*.5+words_deleted[index]*.5)
        index += 1

# Generate the statistics
def generate_statistics():
    if not getgitrep():
        print("Lack of git repository parameter!!")
        sys.exit()
    updaterep()
    get_authors()
    get_filename_n_owner(included)
    get_filename_n_owner(fig,True)    
    remove_fake_commits()
    recovery('innocent_AdaptiveCtrl.txt')
    match_participants()
    generate_git_score()

# Create the html file
def create_html():
    print("Generating HTML file...")
    script_dir= os.path.dirname(__file__)
    relative_path= "html/index.html"
    absolute_path= os.path.join(script_dir, relative_path)
    recent_semester= max([participant.semester for participant in participant_list])
    json_data= []
    file_info= []
    with open(html_dir, "w") as f:
        format= '%Y-%m-%d %H:%M:%S'
        f.write('<!DOCTYPE html>\n')
        f.write('<html>\n')
        f.write('  <head>\n')
        f.write('    <meta charset="UTF-8">\n')
        f.write('    <title>Statistics of Adaptive control</title>\n')
        f.write('    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">\n')
        f.write('		 <link href="https://cdn.datatables.net/v/dt/dt-1.10.16/datatables.min.css" rel="stylesheet" type="text/css" />\n')
        f.write('    <link href="css/table.css" rel="stylesheet" type="text/css" />\n')
        f.write('    <link href="css/container.css" rel="stylesheet" type="text/css" />\n')
        f.write('    <link href="/favicon.png" rel="shortcut icon" type="image/png" />\n')
        f.write('    <script type="text/javascript" src="score.js"></script>\n')
        f.write('    <script type="text/javascript" src="jquery-latest.js"></script>\n')
        f.write('    <script type="text/javascript" src="jquery.tablesorter.js"></script>\n')
        f.write('    <script type="text/javascript">\n')
        f.write('    $(document).ready(function() {\n')
        f.write('			 $(\'tr\').click(function(){json($(this).attr(\'id\'));});')
        f.write('			 $(\'#statistic\').tablesorter();\n')
        f.write('      $(\'#score\').tablesorter();\n')
        f.write('      var years = $(\'td:nth-child(2)\');\n')
        f.write('      $.each(years, function(idx, td_year){\n')
        f.write('        var td_year = $(td_year);\n')
        f.write('        if(td_year.text() != %d) return;\n' % (recent_semester))
        f.write('        \n')
        f.write('        var tr = td_year.parent();\n')
        f.write('        var score = tr.find(\'td:last-child\').text();\n')
        f.write('        if (score >= 70){\n')
        f.write('          tr.children().css(\'background-color\', \'rgb(220, 245, 220)\');\n')
        f.write('        }else{\n')
        f.write('          tr.children().css(\'background-color\', \'rgb(245, 220, 220)\');\n')
        f.write('        }\n')
        f.write('      });\n')
        f.write('      $(\'td:contains("Nordling")\').parent().children().css(\'background-color\', \'rgb(225, 225, 225)\');\n')
        f.write('      $(\'td:contains(".m")\').parent().children().css(\'background-color\', \'rgb(255,255,255)\');\n')
        f.write('      $(\'td:contains(".py")\').parent().children().css(\'background-color\', \'rgb(255,255,255)\');\n')
        f.write('      $(\'td:contains(".tex")\').parent().children().css(\'background-color\', \'rgb(255,255,255)\');\n')
        f.write('    });\n')
        f.write('    </script>\n')
        f.write('  </head>\n')
        f.write('  <body>\n')
        f.write('    <div class="container" id="header">\n')
        f.write('      <h1>Adaptive control %d</h1>\n' % (recent_semester))
        f.write('      <p>Accessed at %s from git repository.</p>\n' \
            % (datetime.datetime.now().strftime(format)))
        f.write('    </div>\n')
        f.write('\n')
        f.write('    <div class="container">\n')
        f.write('      <h2>Statistics of the Commits on Bitbucket</h2>\n')
        f.write('      <table cellspacing="2" id="statistic" class="tablesorter">\n')
        f.write('        <thead>\n')
        f.write('        <tr>\n')
        f.write('          <th>Authors</th>\n')
        f.write('          <th>Semester</th>\n')
        f.write('          <th>Fake Commits</th>\n')
        f.write('          <th>Invalid Commits</th>\n')
        f.write('          <th>Valid Commits</th>\n')
        f.write('          <th>Line Inserted</th>\n')
        f.write('          <th>Line Deleted</th>\n')
        f.write('          <th>Word Inserted</th>\n')
        f.write('          <th>Word Deleted</th>\n')
        f.write('          <th>Total GIT Score</th>\n')
        f.write('        </tr>\n')
        f.write('        </thead>\n')
        f.write('        <tbody>\n')
        for participant in participant_list:
            f.write('          <tr id="%s">\n' % (participant.participant.replace(' ', '')))
            f.write('            <td>%s</td>\n' % (participant.participant))
            f.write('            <td>%d</td>\n' % (participant.semester))
            f.write('            <td>%d</td>\n' % (participant.fake_commits))
            f.write('            <td>%d</td>\n' % (participant.large_commits))
            f.write('            <td>%d</td>\n' % (sum(participant.commits)))
            f.write('            <td>%d</td>\n' % (sum(participant.lines_inserted)))
            f.write('            <td>%d</td>\n' % (sum(participant.lines_deleted)))
            f.write('            <td>%d</td>\n' % (sum(participant.words_inserted)))
            f.write('            <td>%d</td>\n' % (sum(participant.words_deleted)))
            f.write('            <td>%.2f</td>\n' % (participant.git_score))
            f.write('          </tr>\n')
            for participant_file in file_list:
                    if len([i for i in participant.author if i == participant_file.participant])>0:
                            tmp= {}
                            tmp['filename']= participant_file.filename.split('/')[-1]
                            tmp['semester']= participant_file.semester
                            tmp['Fcommits']= participant_file.fake_commits
                            tmp['Icommits']= participant_file.large_commits
                            tmp['Vcommits']= sum(participant_file.commits)
                            tmp['line_ins']= sum(participant_file.lines_inserted)
                            tmp['line_del']= sum(participant_file.lines_deleted)
                            tmp['word_ins']= sum(participant_file.words_inserted)
                            tmp['word_del']= sum(participant_file.words_deleted)
                            tmp['score']= participant_file.git_score
                            file_info.append(tmp)
                            '''
                            f.write('          <tr>\n')
                            f.write('            <td>%s</td>\n'%(participant_file.filename.split('/')[-1]))
                            f.write('            <td>%d</td>\n'%(participant_file.semester))
                            f.write('            <td>%d</td>\n'%(participant_file.fake_commits))
                            f.write('            <td>%d</td>\n'%(participant_file.large_commits))
                            f.write('            <td>%d</td>\n'%(sum(participant_file.commits)))
                            f.write('            <td>%d</td>\n'%(sum(participant_file.lines_inserted)))
                            f.write('            <td>%d</td>\n'%(sum(participant_file.lines_deleted)))
                            f.write('            <td>%d</td>\n'%(sum(participant_file.words_inserted)))
                            f.write('            <td>%d</td>\n'%(sum(participant_file.words_deleted)))
                            f.write('            <td>%.2f</td>\n'%(participant_file.git_score))
                            f.write('          <tr>\n')
                            '''
            element= {}
            element['id'] = participant.participant.replace(' ', '')
            element['files']= file_info
            json_data.append(element)
#            for d in element:
#                print(d['filename'])
            file_info= []
        f.write('        </tbody>\n')
        f.write('      </table>\n')
        if len(author_list) == 0:
            f.write('      <p id="total">&#10004; Total authors: %d </p>\n' % (len(participant_list)))
        else:
            f.write('      <p id="total">&#10006; Total authors: %d </p>\n' % (len(participant_list)))
        f.write('    </div>\n')
        f.write('\n')
        f.write('\n')
        f.write('  </body>\n')
        f.write('</html>\n')
    with open(json_dir, "w") as f:
            json.dump(json_data, f)
    print("")

generate_statistics()
create_html()
