function isNumberKey(evt){
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode != 46 &&(charCode < 48 || charCode > 57)))
        return false;
    return true;
}

function calculate(){	
	var score = document.getElementById("score");
	var num_row = score.rows.length;
	var num_col = score.rows[0].cells.length;
	var weight = [0, 0.18, 0.06, 0.36, 0.1, 0.1, 0.2]
	var temp = 0;
	for (var r = 1; r < num_row; r++) {
		var sum = 0;
		for (var c = 1; c < num_col-2; c++) {
			temp = Number(score.rows[r].cells[c].innerHTML);
			sum += temp*weight[c];
		}
		temp = Number(score.rows[r].cells[num_col-2].children[0].value);
		sum += temp*weight[num_col-2];		
		score.rows[r].cells[num_col-1].innerHTML = Math.round(sum);
    }
}

function json(clicked_id) {
		var file_id = clicked_id + 'Files';
		if ($('#'+clicked_id).closest('tr').next('tr').attr('id').indexOf(file_id)) {
				$.getJSON('./detail.json', function(data) {
						for (var i in data) {
								if (data[i].id == clicked_id) {
										for (var j in data[i].files) {
												$('#'+clicked_id).closest('tr').after(addrows(file_id, data[i].files[j]));
												$('#'+clicked_id).find('#'+file_id).slideDown('slow');
										}
										return;
								}
						}
				});
		}	else {
				$('#'+file_id).slideUp('fast', function() {
						$('tr').remove('#'+file_id);
				});
		}
}

function addrows(id, file_arr) {
		var str = '';
		var style = '<td style="background-color: rgb(255,255,255);">'
		str += '<tr id="'+id+'">';
		str += style + file_arr.filename + '</td>';
		str += style + file_arr.semester.toString() + '</td>';
		str += style + file_arr.Fcommits.toString() + '</td>';
		str += style + file_arr.Icommits.toString() + '</td>';
		str += style + file_arr.Vcommits.toString() + '</td>';
		str += style + file_arr.line_ins.toString() + '</td>';
		str += style + file_arr.line_del.toString() + '</td>';
		str += style + file_arr.word_ins.toString() + '</td>';
		str += style + file_arr.word_del.toString() + '</td>';
		str += style + file_arr.score.toString() + '</td>';
		str += '</tr>';
		return str
}
