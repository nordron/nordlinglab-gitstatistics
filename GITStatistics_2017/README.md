# NordlingLab-GITStatistics #

This is web service for calculating scores base on a GIT log. 
It works with GIT version 1.9.5 and higher.
It consists of three parts: generate_stat.py, fake_commits.txt, and the web page in html folder.

This directory `nordlinglab-gitstatistics/GITStatistics_2017` contains all the code used to generate statistics on contributions to GIT repositories. 

## Usage

* To call the function please use 

		python generate_stat.py $(path to .git)
		
    it will generate an index.html in html folder which can be load into the webserver directly.
    
* To run the code automatically, use crontab to setup a schedule using

        crontab -e
        0 * * * * python path_to_generate_stat path_to_git >| path_to_log 2>&1
        
## Settings

* In fake_commits.txt, we can manually input the commits which will be mark as fake.
* In generate_stat.py, there are several variables can be customize:
    * additional -- Additional options for git log. To exclude file extensions, use -- . ':(exclude)*.xxx' ':(exclude)*.yyy'
    * threshold -- The threshold of words for deciding if a commit is invalid.
    * highest_score -- The index of the score to be set as 100.
    * wt -- The weighting of [commits, lines_inserted, lines_deleted, words_inserted, words_deleted]


## Dependencies

Sorting is enabled by ["TableSorter 2.0 - Client-side table sorting with ease!"](http://tablesorter.com/) 
copyright 2007 by Christian Bach, licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html

TableSorter 2.0 requires ["jQuery JavaScript Library"](http://jquery.com/) 
copyright 2010 by John Resig, licensed under the MIT or GPL Version 2 licenses:
 * http://jquery.org/license

jQuery includes ["Sizzle.js"](http://sizzlejs.com/)
copyright 2010 by The Dojo Foundation, released under the MIT, BSD, and GPL Licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html

A copy of TableSorter 2.0 Version 2.0.5b and jQuery JavaScript Library v1.4.2 are for the convenience of
any user provided with this code, but they are not part of the NordlingLab-GITStatistics package,
nor are they covered by the Apache 2.0 license.

## Background

The statistics is shown at [statistics.nordlinglab.org](statistics.nordlinglab.org). This code was originally developed for showing 
statistics on contributions to [Nordron-SciInfo](https://bitbucket.org/nordron/nordron-sciinfo) during the 
"Scientific information gathering and processing for engineering research" course and part of that repository, but has now been moved to this separate repository. 

## Copyright and license

NordlingLab-GITStatistics - Copyright 2017 Nordron AB - is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)
with a kind request to acknowledge the Nordling Lab if you find it useful. 
We also appreciate pull requests so your features can be incorporated in this package.

### Git repositories that it has been used for

Courses:

1. [Adaptive Control](https://bitbucket.org/temn/nordlinglab-adaptivectrl/)
1. [Introduction to AI](https://bitbucket.org/temn/nordlinglab-introai/)
1. [Professional Skills](https://bitbucket.org/temn/nordlinglab-profskills/)
1. [Scientific Information](https://bitbucket.org/nordron/nordron-sciinfo/)