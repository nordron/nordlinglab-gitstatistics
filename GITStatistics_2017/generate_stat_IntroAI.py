#!/usr/bin/python
#-*- coding: utf-8 -*-

# Copyright 2017 Nordron AB
# Authors: Wei-Yu Lee, Yu-Sin Lin, Torbjorn Nordling
# Email: eric.lee@nordlinglab.org, tn@nordron.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.


# Initialize the variables
git_dir = ""
author_list = []
participant_list = []
time_list = []
class_time = []

# The extension to exclude from git log with the format of ... ':(exclude)*.xxx'
additional = " -- . ':(exclude)*.ilg' ':(exclude)*.ind' ':(exclude)*.ist' ':(exclude)*.lof' ':(exclude)*.log' ':(exclude)*.lot' ':(exclude)*.maf' ':(exclude)*.mtc' ':(exclude)*.mtc1' ':(exclude)*.out' ':(exclude)*.synctex.gz' ':(exclude)*.toc' ':(exclude)*.aux' ':(exclude)*.gz' ':(exclude)*.bbl' ':(exclude)*.blg' ':(exclude)Code/Django/src/SearchDB/calculators/*' ':(exclude)Code/Django/src/SearchDB/common.py' ':(exclude)Code/Django/src/SearchDB/textanalyzer.py'"

# Avoid escape sequence that makes shell unable to find git log
Escape = ['\\a','\\b','\\f','\\r','\\v']

# Threshold for the words in invalid commits
threshold = 3000

# The index of the highest score to use, use 0 for the maximum
highest_score = 0 

# The weighting of [commits, lines_inserted, lines_deleted, words_inserted, words_deleted]
wt = [0.3, 0.2, 0.15, 0.2, 0.15]

# Import packages
import subprocess
import datetime
import sys
import math
import os
import numpy as np
from Participant import Participant

# Print out current participants
def print_Participants(arr):
	print(">> Participants: ")
	for i in range(len(arr)):
			print("%3d  %d  %-20s"%(i, arr[i].semester, arr[i].participant))
	print("")

# Print out the authors which is not assigned to the participants in git log
def print_Authors(arr):
	print(">> Authors Remain: ")
	for i in range(len(arr)):
                        print("%3d  %-50s\t%s"%(i, str(arr[i].author), str(arr[i].commits)))
        print("")

# Remove the last item in a list
def remove_last(arr):
	length = len(arr)
	arr = arr[:length-1]
	return arr

# Get data from cmd command
def getdata(cmd):
	y = cmd[0].decode('string_escape')
	y = y.replace("\\", "\\\\")
	p = subprocess.Popen(y, stdout=subprocess.PIPE, shell=True)
	processes = [p]
	for x in cmd[1:]:
		p = subprocess.Popen(x, stdin=p.stdout, stdout=subprocess.PIPE, shell=True)
        	processes.append(p)
	#for process in processes:
	#	process.wait()
	output = p.communicate()[0]
	return  remove_last(output.split("\n"))

# Get git repository
def getgitrep():
	if len(sys.argv) < 2:
		return False
	global git_dir
	git_dir = sys.argv[1]
	os.chdir(git_dir.split('.git')[0])
	return True

# Pull the latest version in git
def updaterep():
	print("\nUpdating git repository...")
	p = subprocess.Popen("git --git-dir=%s pull origin"%(git_dir), shell=True)
	p.wait()
	print("")

'''
# Get the information about the lines inserted and deleted
def get_lines_data():
	for i in range(len(author_list)):
                cmd=["git --git-dir=%s log --all --numstat --author='%s'%s"%(git_dir, author_list[i].author[0], additional), \
			'grep "^[0-9]"', "awk '{inserted+=$1;deleted+=$2} END {print inserted,deleted}'"]
                (ins, dlt)=getdata(cmd)[0].split(" ")
		try:
			author_list[i].add_lines(int(ins), int(dlt))
		except:
				author_list[i].add_lines(0,0)
'''

# Get the information about the words inserted and deleted per week
def get_words_data():
	first_class_time = datetime.datetime(2019,2,20,03,00) # The first date of the lecture. Note that this is UTC i.e. Taiwan time minus 8 hours
	class_time = [str(first_class_time+datetime.timedelta(i*7))+'Z' for i in range(19)] # The 18 weeks per semester plus the week after the final week.
	for i in range(len(author_list)):
						for j in range(len(class_time)-1):
									cmd1 = ["git --git-dir=%s log -p --all --word-diff=porcelain --since='%s' --until='%s'\
																	--author='%s'%s"%(git_dir,  class_time[j], class_time[j+1],\
																	author_list[i].author[0], additional), 'grep "^+[^+]"', \
																	"awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
									cmd2 = ["git --git-dir=%s log -p --all --word-diff=porcelain --since='%s' --until='%s'\
																	--author='%s'%s"%(git_dir,  class_time[j], class_time[j+1],\
																	author_list[i].author[0], additional), 'grep "^-[^-]"', \
																	"awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
									author_list[i].add_words(int(getdata(cmd1)[0]), int(getdata(cmd2)[0]))

# Get the authors in git history
def get_authors():
	count = 0
	cmd = ["git --git-dir=%s shortlog -sne --all HEAD"%(git_dir), "/usr/bin/awk 'BEGIN{FS=\"\t\"}{print $2,$1}'"]
        author_commits = getdata(cmd)
	print("Getting author information...")
        for x in author_commits:
                (aut, com) = x.split("    ")
		author = Participant()
		author.add_commits(aut, int(com))
		author_list.append(author)
	#get_lines_data()
	get_words_data()
	for x in author_list:
		count += 1
	 #print("%3d  %-55s\t%d\t%d\t%d\t%d"%(count, str(x.author[0]), x.lines_inserted[0], x.lines_deleted[0], x.words_inserted[0], x.words_deleted[0]))
		
		#Author vs (words added+words deleted)
		print("%3d %-55s\t%r"%(count, str(x.author[0]), list(np.add(x.words_inserted,x.words_deleted))))
	print("")

#Rain190111 I comment this part out because we don't need this for ProfSkill class
''' 
# Remove fake and invalid commits
def remove_fake_commits():
	count = 0
	cmd1 = ["git --git-dir=%s log --all%s"%(git_dir, additional), "grep '^commit '"]		
	f = getdata(cmd1)
	for i, c in enumerate(f):
                c = c.replace("\r","")
                c = c.replace(" ","")
                (a, b) = c.split("commit")
                f[i] = b

	script_dir = os.path.dirname(__file__)
        relative_path = "fake_commits.txt"
        absolute_path = os.path.join(script_dir, relative_path)
	cmd2 = ["grep 'commits/' "+absolute_path]
        g = getdata(cmd2)
        for i, c in enumerate(g):
                c = c.replace("\r","")
                c = c.replace(" ","")
		(a, b) = c.split("commits/")
		g[i] = b

	print("Checking fake commits...")
	for commit in f:
		cmd_author = ["git --git-dir=%s log %s -n 1%s"%(git_dir, commit, additional), "grep 'Author:'"]
                cmd_del_word = ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1%s"%(git_dir, commit, additional), \
                        'grep "^-[^-]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                cmd_ins_word = ["git --git-dir=%s log -p --word-diff=porcelain %s -n 1%s"%(git_dir,commit, additional), \
                        'grep "^+[^+]"', "awk '{count+= NF}END{if(count==NULL){print 0}else{print count}}'"]
                cmd_line = ["git --git-dir=%s log --numstat %s -n 1%s"%(git_dir, commit, additional), \
                        'grep "^[0-9]"', "awk '{inserted+=$1;deleted+=$2} END {print inserted,deleted}'"]
		ins_word = int(getdata(cmd_ins_word)[0])
		del_word = int(getdata(cmd_del_word)[0])
		if commit in g or ins_word > threshold or del_word > threshold:
			count += 1
                	(y, z) = getdata(cmd_line)[0].split(" ")
                	(x, aut) = getdata(cmd_author)[0].split("Author: ")
                	# aut = aut.split(" <")[0]
			if commit in g:
				for author in author_list:
					if author.author == [aut]:
						author.remove_commit(1, 0, int(y), int(z), ins_word, del_word)
				print("%3d  %s  %-55s\t%d\t%d\t%d\t%d\t(FAKE)"%(count, str(commit), str(aut), int(y), int(z), ins_word, del_word))
			else:
				for author in author_list:
                	                if author.author == [aut]:
        	                                author.remove_commit(0, 1, int(y), int(z), ins_word, del_word)
	                        print("%3d  %s  %-55s\t%d\t%d\t%d\t%d"%(count, str(commit), str(aut), int(y), int(z), ins_word, del_word))
	print("Total fake commits: %d/%d\n"%(count, len(f)))
'''

# Create participants
def create_participant(semester, participant_name, author_name):
	participant = Participant()
	participant.set_participant(participant_name)
	participant.set_semester(semester)
	for name in author_name:
		for author in author_list:
			if author.author == name:
				participant += author
				author_list.remove(author)
	# Solve double accout problem (Prof Nordling). 36 means 18 weeks times two because of the two account. This code needs to be rephrase. Now it is only for this special case.
	if len(participant.words_inserted)==36:
					# Add the first half and the last half of the words inserted together
					w_in = participant.words_inserted
					w_in_first = w_in[:len(w_in)/2]
					w_in_last = w_in[len(w_in)/2:]
					participant.words_inserted = list(np.add(w_in_first,w_in_last))
					# Add the first half and the last half of the words deleted together
					w_de = participant.words_deleted
					w_de_first = w_de[:len(w_in)/2]
					w_de_last = w_de[len(w_in)/2:]
					participant.words_deleted = list(np.add(w_de_first,w_de_last))
	participant_list.append(participant)
	participant.print_info_profskill()

# Match participants to authors with the form of create_participant(year, 'participant', [author1, author2, ...])
def match_participants():
	create_participant(2019, 'Torbj\xc3\xb6rn Nordling', [['Torbj\xc3\xb6rn Nordling <tn@nordron.com>'], ['Torbj\xc3\xb6rn Nordling <tn@kth.se>']])
	create_participant(2019, 'Gina Chen', [['陳紅瑄 <gina411593@gmail.com>']])
	create_participant(2019, 'Arthur Tseng', [['arthurtsng <e64056423@gs.ncku.edu.tw>']])
	create_participant(2019, 'Meghiya Michelle', [['Meghiya Michelle <meghiyamichelle@gmail.com>']])
	create_participant(2019, 'Pauline Tsai', [['Pauline.Tsai <e64066290@gs.ncku.edu.tw>']])
	create_participant(2019, 'Kevin chen', [['chen <chen.kevinsky@gmail.com>'], ['Kevin Chen <chen.kevinsky@gmail.com>']])
	create_participant(2019, 'Javier Segarra Sanchez-Cutillas', [['Javier Segarra <jsegarra112@gmail.com>']])
	create_participant(2019, 'Sophia Chang', [['張保婕 <sophia000117@gmail.com>']])
	create_participant(2019, 'Xavier Jason Tanes', [['Xavier 陳湧滐 <jojoreivax@gmail.com>']])
	create_participant(2019, 'Abraham Tan', [['Abraham Tan <spongepan2@gmail.com>']])    	
	create_participant(2019, 'James Chen', [['li-pang chen <10310711@gm.clhs.tyc.edu.tw>']])             
	create_participant(2019, 'Howard, Kuan-hao Huang', [['黃冠豪 <khhuang1126@gmail.com>'],['Howard, Kuan-hao Huang <khhuang1126@gmail.com>']])                      
	create_participant(2019, 'Gabrielle Vania Tandjung', [['Gabrielle Vania Tandjung <gabyvchen@gmail.com>']])
	create_participant(2019, 'Mario Yeoh Chuin Shung', [['Mario Yeoh CS <yeohcs88@gmail.com>'], ['yeoh cs <yeohcs88@gmail.com>']])                    
	create_participant(2019, 'David Huang', [['黃士釗 <davidhuang1998@gmail.com>']])                 
	#create_participant(2019, 'Andy Lin', [['AN4056027林崇舜 <an4056027@gs.ncku.edu.tw>']])       
	create_participant(2019, 'Judy Chen', [['Judy Chen <judy453378@gmail.com>']])                  
	create_participant(2019, 'Hanna Marie Dettki', [['hannma <hanna.dettki@gmx.de>'], ['hannama <h.m.dettki@gmail.com>']])                      
	#create_participant(2019, 'Chang Cheng-Han', [['Henry Chang <nemo6302k@gmail.com>']])            
	create_participant(2019, 'Cheng HSI AN', [['Shawnzion <handsomekid999@gmail.com>']])         
	create_participant(2019, 'Jordan Gong', [['JordanGong <j2855001@gmail.com>']]) 
	create_participant(2019, 'Hsieh, Yun-Jung', [['謝昀融 <roy01842@gmail.com>']])               
	#create_participant(2019, 'KUO NI NI', [['郭倪倪 <10310312@gm.clhs.tyc.edu.tw>']])               
	create_participant(2019, 'Rio Ananta Perangin-Angin', [['Rio Ananta Perangin-Angin <erwinkeliat21@gmail.com>']])    	
	create_participant(2019, 'Wang Haw-tien', [['dimitri wang <dimitriwang97@gmail.com>']])            
	create_participant(2019, 'Jose Vicente Ivars Tur', [['Pepe Ivars <pepeivarstur@gmail.com>'], ['Pepe Ivars Tur <pepeivarstur@gmail.com>']])                 
	create_participant(2019, 'Edwin Arkel Rios', [['Edwin Arkel Rios <xedwin23x@gmail.com>'], ['xEdwin23x <xedwin23x@gmail.com>'], ['Edwin A. Rios <xedwin23x@gmail.com>']])               
	create_participant(2019, 'Vukile Pearl Masango', [['Vukile Masango <masangovukile@gmail.com>']])     
	create_participant(2019, 'Jenny Sun', [['L26074043孫于娟 <l26074043@gs.ncku.edu.tw>']])      
	create_participant(2019, 'Bivas Panigrahi', [['Bivas Panigrahi <bivaspanigrahi108@gmail.com>']])  
	#create_participant(2019, 'Mario Yeoh Chuin Shung', [['yeoh cs <yeohcs88@gmail.com>']])  Duplicated
	#create_participant(2019, 'Kevin Chen', [['Kevin Chen <chen.kevinsky@gmail.com>']]) Duplicated
	#create_participant(2019, 'Kevin Shih', [['Kevin Shih <shihkevin1@gmail.com>']])              
	#create_participant(2019, 'Pepe Ivars Tur', [['Pepe Ivars Tur <pepeivarstur@gmail.com>']]) Duplicated
	#create_participant(2019, 'xEdwin23x', [['xEdwin23x <xedwin23x@gmail.com>']])   Duplicated                   
	create_participant(2019, 'Juan Zarate', [['Juan Jose Zarate Monroy <juanjo.zm@gmail.com>']])     
	create_participant(2019, 'Tracy Huang', [['yujiehuang <yujiehuang1118@gmail.com>']])          
	#create_participant(2019, 'Christine Pham', [['Christine Pham <phamlehanhphuong@gmail.com>']])      
	create_participant(2019, 'Meiselina Irmayanti Abdul', [['Meiselina Irmayanti Abdul <meiselina@unib.ac.id>']]) 
	#create_participant(2019, 'Andi Abdul Halik Lateko', [['Andi Abdul Halik Lateko <halik@unismuh.ac.id>']])     
	#create_participant(2019, 'Edwin A. Rios', [['Edwin A. Rios <xedwin23x@gmail.com>']])     Duplicated 
	#create_participant(2019, 'Howard, Kuan-hao Huang', [['Howard, Kuan-hao Huang <khhuang1126@gmail.com>']])  Duplicated
	#create_participant(2019, 'Ha De', [['Ha De <h.m.dettki@gmail.com>']])        Duplicated   
	#create_participant(2019, 'Ke-Erh Huang', [['Ke-Erh Huang <huangkeerh@gmail.com>']])
	
	print_Participants(participant_list)	
	print_Authors(author_list)

# Calculate the score of each participant
def score_func(arr, val):
	arr_sum = sum(arr)
	if arr_sum > 0:
		score = math.log10(arr_sum/float(val))
	else:
		score = -10
	return score

# Calculate git score with git log
def generate_git_score():
	index = 0
	prof = []
	commits = []
	lines_inserted = []
	lines_deleted = []
	words_inserted = []
	words_deleted = []
	AttendanceScore = []
	for participant in participant_list:
		if participant.participant == 'Torbj\xc3\xb6rn Nordling':
			prof = [sum(participant.commits), sum(participant.lines_inserted), sum(participant.lines_deleted), \
                                sum(participant.words_inserted), sum(participant.words_deleted)]
	for participant in participant_list:
		if not participant.participant == 'Torbj\xc3\xb6rn Nordling':
			commits.append(score_func(participant.commits, prof[0]))
			lines_inserted.append(score_func(participant.lines_inserted, prof[1]))
			lines_deleted.append(score_func(participant.lines_deleted, prof[2]))
			words_inserted.append(score_func(participant.words_inserted, prof[3]))
			words_deleted.append(score_func(participant.words_deleted, prof[4]))
	#maximum = [i[highest_score] for i in sorted_list]
	#maximum = [score_func([88], prof[0]), score_func([2104], prof[1]), score_func([1843], prof[2]), score_func([16609], prof[3]), score_func([6115], prof[4])]
	'''
	for i in range(len(participant_list)-1):
		if commits[i] <= maximum[0]:
			commits[i] = 70+30*(commits[i]/float(maximum[0]))
		else:
			commits[i] = 100
		if lines_inserted[i] <= maximum[1]:
			lines_inserted[i] = 70+30*(lines_inserted[i]/float(maximum[1]))
                else:
                        lines_inserted[i] = 100
		if lines_deleted[i] <= maximum[2]:
			lines_deleted[i] = 70+30*(lines_deleted[i]/float(maximum[2]))
                else:
			lines_deleted[i] = 100
		if words_inserted[i] <= maximum[3]:
			words_inserted[i] = 70+30*(words_inserted[i]/float(maximum[3]))
                else:
			words_inserted[i] = 100
		if words_deleted[i] <= maximum[4]:
			words_deleted[i] = 70+30*(words_deleted[i]/float(maximum[4]))
                else:
			words_deleted[i] = 100
			'''
	# Attendance comes from goole form "Lecture_attendance"
	Attendance = [18, 17, 17, 16, 14, 17, 14, 16, 15, 15, 17, 13, 9, 13, 17, 15, 18, 9]
	for i in range(len(Attendance)):
					AttendanceScore.append(2.83*np.exp(0.03565*Attendance[i]/18))
	print(AttendanceScore)
	# Git score
	for participant in participant_list:
                if not participant.participant == 'Torbj\xc3\xb6rn Nordling':
			#participant.set_git_score(commits[index]*wt[0]+lines_inserted[index]*wt[1]+lines_deleted[index]*wt[2]+ words_inserted[index]*wt[3]+words_deleted[index]*wt[4])
			NetWordCount = sum(participant.words_inserted)+sum(participant.words_deleted)
			participant.set_git_score(9.3*np.log(9.35*NetWordCount))
			index += 1
		else:
			participant.set_git_score(60)

# Generate the statistics
def generate_statistics():
	if not getgitrep():
		print("Lack of git repository parameter!!")
		sys.exit()
	updaterep()
	get_authors()
	#remove_fake_commits()
	match_participants()
	generate_git_score()

#======================================================================================================
# Create the html file
def create_html():
	print("Generating HTML file...")
	script_dir = os.path.dirname(__file__)
	relative_path = "html/index.html"
	absolute_path = os.path.join(script_dir, relative_path)
	recent_semester = max([participant.semester for participant in participant_list])
	with open(absolute_path, "w") as f:
		format = '%Y-%m-%d %H:%M:%S'
		f.write('<!DOCTYPE html>\n')
		f.write('<html>\n')
		f.write('  <head>\n')
		f.write('    <meta charset="UTF-8">\n')
		f.write('    <title>Statistics</title>\n')
		f.write('    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">\n')
		f.write('    <link href="css/table.css" rel="stylesheet" type="text/css" />\n')
		f.write('    <link href="css/container.css" rel="stylesheet" type="text/css" />\n')
		f.write('		 <link href="/favicon.png" rel="shortcut icon" type="image/png" />\n')
		f.write('    <script type="text/javascript" src="score.js"></script>\n')
		f.write('    <script type="text/javascript" src="jquery-latest.js"></script>\n')
		f.write('    <script type="text/javascript" src="jquery.tablesorter.js"></script>\n')
		f.write('    <script type="text/javascript">\n')
		f.write('    $(document).ready(function() {\n')
		f.write('      $(\'#statistic\').tablesorter();\n')
		f.write('      $(\'#score\').tablesorter();\n')
		f.write('      $(\'td:contains("Nordling")\').parent().children().css(\'background-color\', \'rgb(225, 225, 225)\');\n')
		f.write('      var years = $(\'td:nth-child(2)\');\n')
		f.write('      $.each(years, function(idx, td_year){\n')
		f.write('        var td_year = $(td_year);\n')
		f.write('        if(td_year.text() != %d) return;\n'%(recent_semester))
		f.write('        \n')
		f.write('        var tr = td_year.parent();\n')
		f.write('        var score = tr.find(\'td:last-child\').text();\n')
		f.write('        if (score >= 60){\n')
		f.write('          tr.children().css(\'background-color\', \'rgb(220, 245, 220)\');\n')
		f.write('        }else{\n')
		f.write('          tr.children().css(\'background-color\', \'rgb(220, 245, 220)\');\n')  # Do not change color by score by Butters
		f.write('        }\n')
		f.write('      });\n')
		f.write('    });\n')
		f.write('    </script>\n')
		f.write('  </head>\n')
		f.write('  <body>\n')
		f.write('    <div class="container" id="header">\n')
		f.write('      <h1>Introduction to artificial intelligence and deep learning for engineers and scientists</h1>\n')
		f.write('      <p>Accessed at %s from git repository.</p>\n' \
			%(datetime.datetime.now().strftime(format)))
		f.write('    </div>\n')
		f.write('\n')
		f.write('    <div class="container">\n')
		f.write('      <h2>Statistics of the Commits on Bitbucket</h2>\n')
		f.write('      <table cellspacing="2" id="statistic" class="tablesorter">\n')
		f.write('        <thead>\n')
		f.write('        <tr>\n')
		f.write('          <th>Authors</th>\n')
		f.write('          <th>Semester</th>\n')
		for i in range(18):
						f.write('     <th>Week %d</th>\n'%(i+1))
		#f.write('          <th>Word Inserted</th>\n')
		#f.write('          <th>Word Deleted</th>\n')
		f.write('          <th>Net word count</th>\n')
		f.write('          <th>Weeks with commits</th>\n')
		#f.write('          <th>GIT Score</th>\n') No git score by Butters
		f.write('        </tr>\n')
		f.write('        </thead>\n')
		f.write('        <tbody>\n')

		for participant in participant_list:
			f.write('          <tr>\n')
			f.write('            <td>%s</td>\n'%(participant.participant))
			f.write('            <td>%d</td>\n'%(participant.semester))

			for i in range(18): # The 18 weeks per semester
							#print(participant.words_inserted[i])
							#print(participant.words_deleted[i])
							f.write('            <td>%d</td>\n'%((participant.words_inserted[i])+participant.words_deleted[i]))
			#f.write('            <td>%d</td>\n'%(participant.large_commits))
			#f.write('            <td>%d</td>\n'%(sum(participant.commits)))
			#f.write('            <td>%d</td>\n'%(sum(participant.lines_inserted)))
			#f.write('            <td>%d</td>\n'%(sum(participant.lines_deleted)))
			#f.write('            <td>%d</td>\n'%(sum(participant.words_inserted)))
			#f.write('            <td>%d</td>\n'%(sum(participant.words_deleted)))
			f.write('            <td>%d</td>\n'%((sum(participant.words_inserted)+sum(participant.words_deleted))))	
			f.write('            <td>%.2f</td>\n'%(np.count_nonzero(list(np.add(participant.words_inserted,participant.words_deleted)))))
			#f.write('            <td>%.2f</td>\n'%(participant.git_score)) No git score by Butters
			f.write('          </tr>\n')

		f.write('        </tbody>\n')
		f.write('      </table>\n')
		if len(author_list) == 0:
			f.write('      <p id="total">&#10004; Total authors: %d </p>\n'%(len(participant_list)))
		else:
			f.write('      <p id="total">&#10006; Total authors: %d </p>\n'%(len(participant_list)))
		f.write('      <p>Note that merges won\'t give unfair points.</p>\n')
		f.write('    </div>\n')
		f.write('\n')
#		f.write('    <div class="container" id="score_div">\n')
#		f.write('      <h2>Total Score for the Course</h2>\n')
#		f.write('      <button class="btn" onclick="calculate()">Calculate Total Score</button>\n')
#		f.write('      <table cellspacing="2" id="score" class="tablesorter">\n')
#		f.write('        <thead>\n')
#		f.write('          <tr>\n')
#		f.write('            <th>Participants</th>\n')
#		f.write('            <th>Attendace at lecture</th>\n')
#		f.write('            <th>Attendance at daily scrum</th>\n')
#		f.write('            <th>GIT Score</th>\n')
#		f.write('            <th>Oral presentation</th>\n')
#		f.write('            <th>Quiz Score</th>\n')
#		f.write('            <th>Report Score</th>\n')
#		f.write('            <th>Total Score</th>\n')
#		f.write('          </tr>\n')
#		f.write('        </thead>\n')
#		f.write('        <tbody>\n')
#		
#		for participant in participant_list:
#                        f.write('          <tr>\n')
#                        f.write('            <td>%s</td>\n'%(participant.participant))
#                        f.write('            <td>%d</td>\n'%(participant.semester))
#                        f.write('            <td>%d</td>\n'%(0))
#                        f.write('            <td>%d</td>\n'%(0))
#                        f.write('            <td>%d</td>\n'%(0))
#                        f.write('            <td>%d</td>\n'%(0))
#			f.write('            <td>\n')
#			f.write('              <input type="text" value="85" onkeypress="return isNumberKey(event)" maxlength="3">\n')
#			f.write('            </td>\n')
#                        f.write('            <td>%d</td>\n'%(0))
#                        f.write('          </tr>\n')
#
#		f.write('        </tbody>\n')
#		f.write('      </table>\n')
#		f.write('    </div>\n')
		f.write('\n')
		f.write('  </body>\n')
		f.write('</html>\n')	

	print("")

generate_statistics()
create_html()
