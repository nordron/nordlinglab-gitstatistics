#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright 2017 Nordron AB
# Authors: Wei-Yu Lee, Yu-Sin Lin, Torbjorn Nordling
# Email: eric.lee@nordlinglab.org, tn@nordron.com
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

# Setup the structure of participant


class Participant():
    def __init__(self):
        self.participant = []
        self.semester = []
        self.fake_commits = 0
        self.large_commits = 0
        self.git_score = 0
        self.author = []
        self.commits = []
        self.lines_inserted = []
        self.lines_deleted = []
        self.lines_inserted_fake = []
        self.lines_deleted_fake = []
        self.words_inserted = []
        self.words_deleted = []
        self.words_inserted_fake = []
        self.words_deleted_fake = []

    def set_participant(self, participant):
        self.participant = participant

    def set_semester(self, semester):
        self.semester = semester

    def set_git_score(self, score):
        self.git_score = score

    def add_commits(self, author, commits):
        self.author.append(author)
        self.commits.append(commits)

    def add_lines(self, lines_inserted, lines_deleted):
        self.lines_inserted.append(lines_inserted)
        self.lines_deleted.append(lines_deleted)

    def add_words(self, words_inserted, words_deleted):
        self.words_inserted.append(words_inserted)
        self.words_deleted.append(words_deleted)

    def remove_commit(self, fake_commits, large_commits, lines_inserted, lines_deleted, words_inserted, words_deleted):
        self.fake_commits += fake_commits
        self.large_commits += large_commits
        self.commits[0] -= large_commits+fake_commits
        self.lines_inserted[0] -= lines_inserted
        self.lines_deleted[0] -= lines_deleted
        self.words_inserted[0] -= words_inserted
        self.words_deleted[0] -= words_deleted
        if fake_commits:
            try:
                self.lines_inserted_fake[0] += lines_inserted
            except:
                self.lines_inserted_fake.append(lines_inserted)
            try:
                self.lines_deleted_fake[0] += lines_deleted
            except:
                self.lines_deleted_fake.append(lines_deleted)
            try:
                self.words_inserted_fake[0] += words_inserted
            except:
                self.words_inserted_fake.append(words_inserted)
            try:
                self.words_inserted_fake[0] += words_deleted
            except:
                self.words_inserted_fake.append(words_deleted)

    def restore_commit(self, lines_inserted, lines_deleted, words_inserted, words_deleted):
        self.lines_inserted.append(lines_inserted)
        self.lines_deleted.append(lines_deleted)
        self.words_inserted.append(words_inserted)
        self.words_deleted.append(words_deleted)
        self.commits.append(1)
        self.large_commits -= 1

    def __add__(self, other):
        self.fake_commits += other.fake_commits
        self.large_commits += other.large_commits
        self.author.extend(other.author)
        self.commits.extend(other.commits)
        self.lines_inserted.extend(other.lines_inserted)
        self.lines_deleted.extend(other.lines_deleted)
        self.lines_inserted_fake.extend(other.lines_inserted_fake)
        self.lines_deleted_fake.extend(other.lines_deleted_fake)
        self.words_inserted.extend(other.words_inserted)
        self.words_deleted.extend(other.words_deleted)
        self.words_inserted_fake.extend(other.words_inserted_fake)
        self.words_deleted_fake.extend(other.words_deleted_fake)
        return self

    def __iadd__(self, other):
        self.fake_commits += other.fake_commits
        self.large_commits += other.large_commits
        self.author.extend(other.author)
        self.commits.extend(other.commits)
        self.lines_inserted.extend(other.lines_inserted)
        self.lines_deleted.extend(other.lines_deleted)
        self.lines_inserted_fake.extend(other.lines_inserted_fake)
        self.lines_deleted_fake.extend(other.lines_deleted_fake)
        self.words_inserted.extend(other.words_inserted)
        self.words_deleted.extend(other.words_deleted)
        self.words_inserted_fake.extend(other.words_inserted_fake)
        self.words_deleted_fake.extend(other.words_deleted_fake)
        return self

    def print_info(self):
        print("Participant:\t%s" % (str(self.participant)))
        print("Semester:\t%s" % (str(self.semester)))
        print("Git score:\t%s" % (str(self.git_score)))
        print("Fake commits:\t%s" % (str(self.fake_commits)))
        print("Large commits:\t%s" % (str(self.large_commits)))
        print("Author:\t\t%s" % (str(self.author)))
        print("Commits:\t%s" % (str(self.commits)))
        print("Lines inserted:\t%s" % (str(self.lines_inserted)))
        print("Lines deleted:\t%s" % (str(self.lines_deleted)))
        print("Words inserted:\t%s" % (str(self.words_inserted)))
        print("Words deleted:\t%s" % (str(self.words_deleted)))
        print("")

    def print_info_profskill(self):
        print("Participant:\t%s" % (str(self.participant)))
        print("Semester:\t%s" % (str(self.semester)))
        print("Git score:\t%s" % (str(self.git_score)))
        print("Author:\t\t%s" % (str(self.author)))
        print("Commits:\t%s" % (str(self.commits)))
        print("Words inserted:\t%s" % (str(self.words_inserted)))
        print("Words deleted:\t%s" % (str(self.words_deleted)))
        print("")

class Files(Participant):
    def __init__(self):
        Participant.__init__(self)
        self.filename = []

    def set_filename(self, filename):
        self.filename = filename
